@isTest
private class CaseTriggerHandler_Test {
    @isTest
    static void testCaseCreation() {
        //create test person account
        List<Account> testPersonAccounts = TestDataFactory.initializePersonAccounts(1);
        insert testPersonAccounts;
        Id personContactId = [Select PersonContactId from Account where Id =:testPersonAccounts[0].Id].PersonContactId;

        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> testCases = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 200);
        for(Case c: testCases){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Sub_Type__c = 'Refusal';
            c.Refusal_Reason__c = 'Perception';
            c.Resolution_Notes__c = 'Test';
            c.Case_Phone_Number__c = 'Use main phone instead';
            c.Type_of_Resolution__c = 'No';
        }
        Test.startTest();
        insert testCases;
        Test.stopTest();

        //assert counters are updated
        //System.assert([Select Strike__c from Case limit 1].Strike__c == 1, 'Strike Count should have been incremented');
        //System.assert([Select Strike_Counter__pc from Account limit 1].Strike_Counter__pc == 200, 'Strike Count should have been incremented as a result of all the cases created');
    }

     @isTest
    static void testCaseUpdateNPS() {
        //create test person account
        List<Account> testPersonAccounts = TestDataFactory.initializePersonAccounts(1);
        insert testPersonAccounts;
        Id personContactId = [Select PersonContactId from Account where Id =:testPersonAccounts[0].Id].PersonContactId;
        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> testCases = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 1);
        for(Case c: testCases){
            c.Type = 'Pre Delivery';
            c.Sub_Type__c = 'Reschedule';
            c.Survey_Opt_In__c = true;
            c.Type_of_Resolution__c = 'Yes';
        }
        
        //update case with  survey opt in checkbox
        Test.startTest();
        insert testCases;
        for(Case c: testCases){
            c.Survey_Opt_In__c = false;
            
        }
        update testCases;
        Test.stopTest();
    }
    
    @isTest
    static void testCaseUpdate() {
        //create test person account
        List<Account> testPersonAccounts = TestDataFactory.initializePersonAccounts(1);
        insert testPersonAccounts;
        Id personContactId = [Select PersonContactId from Account where Id =:testPersonAccounts[0].Id].PersonContactId;

        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> testCases = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 50);
        for(Case c: testCases){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Type_of_Resolution__c = 'test';
        }
        insert testCases;

        //update case with another sub type
        Test.startTest();
        for(Case c: testCases){
            c.Sub_Type__c = null;
        }
        update testCases;

        for(Case c: testCases){
            c.Sub_Type__c = CaseTriggerHandler.CASE_SUB_TYPE_DAMAGE;
        }
        update testCases;
        
        for(Case c: testCases){		
            c.Type = 'Post Delivery';		
            c.Sub_Type__c = 'Changed Mind';		
            c.Type_of_Resolution__c='Yes';		
            c.Market__c = 'ASHCOMM';		
        }		
        update testCases;
        
        for(Case c: testCases){		
            c.Type = 'Post Delivery';		
            c.Sub_Type__c = 'FPP';		
            c.Type_of_Resolution__c='Yes';		
            c.Market__c = '';		
        }		
        update testCases;
        
        for(Case c: testCases){		
            c.Type = 'Post Delivery';		
            c.Sub_Type__c = 'FPP';		
            c.Type_of_Resolution__c='';		
            c.Market__c = '';		
        }		
        update testCases;
        Test.stopTest();

        //assert counters are updated
        System.assert([Select Strike__c from Case limit 1].Strike__c == 2, 'Strike Count should have been incremented');
        //System.assert([Select Strike_Counter__c from Contact limit 1].Strike_Counter__c == 400, 'Strike Count should have been incremented');
    }
    
     @isTest
    static void testCaseUpdate1() {
        
        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> mycaselist = new List<Case>();
        Case c1 = new Case(RecordTypeId=serviceRequestRecordTypeId,
                           Type = 'Post Delivery',
                           Sub_Type__c = 'FPP',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'test',
                           Market__c = 'Southwest Phoenix - #8888000');
        mycaselist.add(c1);
        Case c2 = new Case(RecordTypeId=serviceRequestRecordTypeId,
                           Type = 'Post Delivery',
                           Sub_Type__c = 'FPP',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'test');
        mycaselist.add(c2);
        Case c3 = new Case(RecordTypeId=serviceRequestRecordTypeId,
                           Type = 'Delivery',
                           Sub_Type__c = 'Changed Mind',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'test');
        mycaselist.add(c3);
        Case c4 = new Case(RecordTypeId=serviceRequestRecordTypeId,
                           Type = 'Delivery',
                           Sub_Type__c = 'Changed Mind',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c4);
        Case c5 = new Case(RecordTypeId=serviceRequestRecordTypeId,
                           Type = 'Delivery',
                           Sub_Type__c = 'Wrong Item',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes');
        mycaselist.add(c5);
        Case c6 = new Case(RecordTypeId=serviceRequestRecordTypeId,
                           Type = 'Delivery',
                           Sub_Type__c = 'Wrong Item',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c6);
        Case c7 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Address Change',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c7);
        Case c8 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Address Change',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes',
                           Market__c = 'ASHCOMM');
        mycaselist.add(c8);
        Case c9 = new Case(Type = 'Delivery',
                           Sub_Type__c = 'Not At Home',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c9);
        Case c10 = new Case(Type = 'Delivery',
                           Sub_Type__c = 'Not At Home',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes');
        mycaselist.add(c10);
        Case c11 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Service Add On/Removal',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c11);
        Case c12 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Service Add On/Removal',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes');
        mycaselist.add(c12);
        Case c13 = new Case(Type = 'Post Delivery',
                           Sub_Type__c = 'FPP',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c13);
        Case c14 = new Case(Type = 'Post Delivery',
                           Sub_Type__c = 'FPP',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes');
        mycaselist.add(c14);
        Case c15 = new Case(Type = 'Delivery',
                           Sub_Type__c = 'FPP',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c15);
        Case c16 = new Case(Type = 'Delivery',
                           Sub_Type__c = 'FPP',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes');
        mycaselist.add(c16);
        Case c17 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Product/ Price Change',
                           Type_of_Resolution__c='Yes');
        mycaselist.add(c17);
        Case c18 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Product/ Price Change',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes');
        mycaselist.add(c18);
        Case c19 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Cancellation',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes');
        mycaselist.add(c19);
        Case c20 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Cancellation',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes',
                           Market__c = 'ASHCOMM');
         mycaselist.add(c20);
        Contact con  = new Contact(FirstName = 'Test',LastName = 'Contact');
        insert con;              
         Case c21 = new Case(Type = 'Pre-Delivery',
                           Sub_Type__c = 'Cancellation',
                           Type_of_Resolution__c='No',
                           Resolution_Notes__c = 'TEST Res Notes',
                           ContactId = con.Id,  
                           Market__c = 'Rockledge Chicago - #8888400',
                           Special_Processing__c = 'Sooner Date',
                           What_service_is_needed_for_each_SKU__c = 'Test',
                           Desired_Service_Date__c = System.today());
      
        mycaselist.add(c21);
        Test.startTest();
        insert mycaselist;
        Test.stopTest();        
        //update case with another type/sub type
        
        //System.assert([Select Strike__c from Case limit 1].Strike__c == 2, 'Strike Count should have been incremented');
    }
    
    @isTest
    static void testSalesOrderUpdateOnCaseInsert() {
        //create test person account
        List<Account> testPersonAccounts = TestDataFactory.initializePersonAccounts(1);
        insert testPersonAccounts;
        Id personContactId = [Select PersonContactId from Account where Id =:testPersonAccounts[0].Id].PersonContactId;

        //get id prefix for sales orders
        Schema.DescribeSObjectResult describeResult = SalesOrder__x.SobjectType.getDescribe();
        string salesOrderIdPrefix = describeResult.getKeyPrefix();
        string smapleSalesOrderId = salesOrderIdPrefix + '2F0000004eetAAA';

        SalesOrder__x sampleOrder = new SalesOrder__x(Id = smapleSalesOrderId, 
                                                      ExternalId = 'test000000000000000000000', 
                                                      phhProfitcenter__c = 3,
                                                      phhStoreLocation__c = 'Shadow Mountain', 
                                                      phhStoreID__c = '943',
                                                      fulfillerID__c = '8888000-570');
        SalesOrderDAO.mockedSalesOrders.add(sampleOrder);

        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> testCases = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 50);
        for(Case c: testCases){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Sales_Order__c = smapleSalesOrderId;
            c.Type_of_Resolution__c = 'Exchange';
        }
        //set the first case with temp sales order field populated (this happens when a case is creted from an Order screen)
        testCases[0].Temp_Sales_Order__c = 'test000000000000000000000';

        List<Case> testCases1 = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 50);
        for(Case c: testCases1){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Sales_Order__c = smapleSalesOrderId;
            c.Type_of_Resolution__c = 'test';
        }
        //set the first case with temp sales order field populated (this happens when a case is creted from an Order screen)
        testCases1[0].Temp_Sales_Order__c = '';
        
        Test.startTest();
        insert testCases;
        insert testCases1;
        Test.stopTest();

        for(Case savedCopy : [Select Sales_Order__c from Case where Id in: testCases]){
            //System.assert(savedCopy.Sales_Order__c == 'test000000000000000000000', 'External id of the sales order should have been populated');
        }
        for(Case savedCopy1 : [Select Sales_Order__c from Case where Id in: testCases1]){
            
        }
    }

    @isTest
    static void updateSuppliedEmailAndCaseStatusTest() {
        //create test person account
        List<Account> testPersonAccounts = TestDataFactory.initializePersonAccounts(1);
        insert testPersonAccounts;
        Id personContactId = [Select PersonContactId from Account where Id =:testPersonAccounts[0].Id].PersonContactId;

        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> testCases = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 2);
        for(Case c: testCases){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Close_Case_Quick_Action__c = true;
            c.Type_of_Resolution__c = 'Yes';
            c.Origin = CaseTriggerHandler.CASE_ORIGIN_EMAIL;
            c.Status = CaseTriggerHandler.CASE_STATUS_CLOSED;
            c.Legacy_Account_Ship_To__c = '8888300-164';
            c.Legacy_Service_Request_ID__c = '12345';
        }
        Test.startTest();
        insert testCases;
        Test.stopTest();
    }

    @isTest
    static void closeConsumerAffairsCasesTest() {
        List<Account> testPersonAccounts = TestDataFactory.initializePersonAccounts(1);
        insert testPersonAccounts;
        Id personContactId = [Select PersonContactId from Account where Id =:testPersonAccounts[0].Id].PersonContactId;

        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> testCases = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 1);
        for(Case c: testCases){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Origin = system.label.Consumer_Affairs_Origin;
            c.OwnerId = system.label.Consumer_Affairs_Owner_Id;
        }
        
        List<Case> testCases1 = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 1);
        for(Case c: testCases1){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Type_of_Resolution__c = 'Yes';
            c.Origin = system.label.Consumer_Affairs_Origin;
            c.OwnerId = system.label.Consumer_Affairs_Owner_Id;
            c.IsEscalated = true;
        }
        
        List<Case> testCases2 = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 1);
        for(Case c: testCases2){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Type_of_Resolution__c = 'Yes';
            c.Origin = system.label.Consumer_Affairs_Origin;
            c.OwnerId = system.label.Consumer_Affairs_Owner_Id;
            c.Gift_Card__c = true;
            c.IsEscalated = true;
        }
        
        List<Case> testCases3 = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 1);
        for(Case c: testCases3){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Type_of_Resolution__c = 'Yes';
            c.Origin = 'Email';
            c.OwnerId = system.label.Consumer_Affairs_Owner_Id;
            c.Gift_Card__c = true;
        }
       
        try{ 
        Test.startTest();
        insert testCases;
        insert testCases1;
        insert testCases2;
        insert testCases3;
        Case c1 = new Case();
        c1 = testCases3[0];
        c1.Type_of_Resolution__c = 'Exchange';
        c1.Resolution_Notes__c = 'test';
        update c1;
        Test.stopTest();
        }
        catch(exception ex){
            
        }
        
    }
    
    @isTest
    static void techScheduleTest() {
        //test method to cover tech schedule process in case trigger handler class
        List<Account> testPersonAccounts = TestDataFactory.initializePersonAccounts(1);
        insert testPersonAccounts;
        Id personContactId = [Select PersonContactId from Account where Id =:testPersonAccounts[0].Id].PersonContactId;

        Id serviceRequestRecordTypeId = Utilities_SalesAndService.getRecordTypeId(Case.SobjectType, CaseTriggerHandler.CASE_RECORD_TYPE_SERVICE_REQUEST_DEVELOPER_NAME);
        List<Case> testCases = TestDataFactory.initializeCases(testPersonAccounts[0].Id, personContactId, 2);
        integer i=0;
        for(Case c: testCases){
            c.RecordTypeId = serviceRequestRecordTypeId;
            c.Type_of_Resolution__c = 'Yes';
            if (i == 0) {
                ++i;
                c.Technician_Schedule_Date__c = system.today();
            } else {
                c.Legacy_Account_Ship_To__c = '8888300-164';
                c.Tech_Scheduled_Date__c = system.today();
            }
        }
        insert testCases;
    }
}