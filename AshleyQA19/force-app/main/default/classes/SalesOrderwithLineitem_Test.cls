@isTest
public class SalesOrderwithLineitem_Test {
	public static testMethod void testsalesItemExport() {
        Id cId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Account acc = new Account();
        //acc.name = 'Acc Name';
        acc.LastName = 'Account Name';
        acc.BillingCity = 'testcity';
        acc.BillingPostalCode = '67872';
        acc.BillingState = 'teststate';
        acc.BillingStreet = 'teststreet';
        acc.PersonEmail = 'test@test.com';
        acc.Phone = '9876543210';
        acc.RecordTypeId = cId;
        insert acc;
        Opportunity opp = new Opportunity();
        
        opp.AccountId = acc.Id;
        opp.StageName = 'Saved Shopping Cart';
        opp.Name = 'Testing opp';
        opp.CloseDate = System.today() + 5;
        insert opp;
        
        Shopping_cart_line_item__c shopcartline = new Shopping_cart_line_item__c();
        
        shopcartline.PRODUCT_SKU__C = '34-546';
        shopcartline.Product_Title__c = 'Test Product Title'; 
        shopcartline.DeliveryDate__c = system.today() + 3;
        shopcartline.Opportunity__c = opp.Id;
        
        insert shopcartline;
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/SalesOrderLineItemExportCSV';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        List<String> stas = SalesOrderwithLineitem.getOrderValues();                
        Test.stopTest();
    }
}