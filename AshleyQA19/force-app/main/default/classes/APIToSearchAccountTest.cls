@isTest
public class APIToSearchAccountTest {
    @isTest static void testMethod1(){
        //Create Account
        Account delacct = new Account();
       // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
        delacct.Primary_Language__pc = 'English';
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone2 = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone3 = '889' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        delacct.phone = phone;
        delacct.Phone_2__pc = phone2;
        delacct.Phone_3__pc = phone3;
        delacct.PersonEmail = 'pqr@mm.com';
        delacct.Email_2__pc ='pqr@mm.com';
        delacct.Strike_Counter__pc = 10;
        Insert delacct;
        System.debug('delacct-->'+delacct.Id);
        
        Account mergeacct = new Account();
       // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        mergeacct.LastName = 'last' + Math.random();
        mergeacct.FirstName = 'first' + Math.random();
        mergeacct.PersonEmail = mergeacct.FirstName + '.' + mergeacct.LastName + '@test.com';
        mergeacct.Primary_Language__pc = 'English';
        mergeacct.Strike_Counter__pc = 2;
        Insert mergeacct;
                        
        //Create Case
        Case c = new Case();
        c.Status = 'Open';
        c.Priority = 'Medium';
        c.Origin = 'Web';
        c.Subject = 'Subject ' +  Math.random();
        c.Type = 'Delivery_Order_inquiry';
        c.Sub_Type__c = CaseTriggerHandler.CASE_SUB_TYPE_DAMAGE;			
        c.AccountId = delacct.Id;
       // c.Sales_Order__c = '122324:332536575';
        c.Tech_Scheduled_Date__c = Date.today()+2;
        Insert c;
               
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/v1/SearchAccounts/firstname='+ delacct.FirstName +'+lastname='+delacct.LastName+'+phone='+phone+'+email='+delacct.PersonEmail; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        APIToSearchAccount.AccountWrapper resfinal = APIToSearchAccount.doGet();
       // System.debug(resmsg);
   		Test.stopTest();
        
    }
    @isTest static void testMethod2(){
        //Create Account
        Account delacct = new Account();
       // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
        delacct.Primary_Language__pc = 'English';
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone2 = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone3 = '889' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        delacct.phone = phone;
        delacct.Phone_2__pc = phone2;
        delacct.Phone_3__pc = phone3;
        delacct.PersonEmail = 'pqr@mm.com';
        delacct.Email_2__pc ='pqr@mm.com';
        delacct.Strike_Counter__pc = 10;
        Insert delacct;
        System.debug('delacct-->'+delacct.Id);
        
        Account mergeacct = new Account();
       // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        mergeacct.LastName = 'last' + Math.random();
        mergeacct.FirstName = 'first' + Math.random();
        mergeacct.PersonEmail = mergeacct.FirstName + '.' + mergeacct.LastName + '@test.com';
        mergeacct.Primary_Language__pc = 'English';
        mergeacct.Strike_Counter__pc = 2;
        Insert mergeacct;
                        
        //Create Case
        Case c = new Case();
        c.Status = 'Open';
        c.Priority = 'Medium';
        c.Origin = 'Web';
        c.Subject = 'Subject ' +  Math.random();
        c.Type = 'Delivery_Order_inquiry';
        c.Sub_Type__c = CaseTriggerHandler.CASE_SUB_TYPE_DAMAGE;			
        c.AccountId = delacct.Id;
       // c.Sales_Order__c = '122324:332536575';
        c.Tech_Scheduled_Date__c = Date.today()+2;
        Insert c;
               
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/v1/SearchAccounts/firstname='+ delacct.FirstName +'+lastname='+delacct.LastName+'+phone='+phone; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        APIToSearchAccount.AccountWrapper resfinal = APIToSearchAccount.doGet();
       // System.debug(resmsg);
   		Test.stopTest();
        
    }

}