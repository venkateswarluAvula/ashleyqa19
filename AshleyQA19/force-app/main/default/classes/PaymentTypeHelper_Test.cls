/**************************************************************************************************
* Name       : PaymentTypeHelper_Test
* Purpose    : Test class for PaymentTypeHelper
***************************************************************************************************
* Author            | Version    | Created Date    | Description
***************************************************************************************************
*Sridhar            | 1.0        | 3/2/2018       | Initial Draft
**************************************************************************************************/
@isTest
public class PaymentTypeHelper_Test {
    @testSetup
    public static void initsetup(){
        
        TestDataFactory.prepareAPICustomSetting();
    }
    
    @isTest
    public static void PositiveWay(){
        Payment_Type__c pMap = new Payment_Type__c();
        pMap.TenderCode__c = 'AMEX';
        pMap.TenderCodeDescription__c = 'American Express Card';
        pMap.DocumentNumber__c = '300';
        date curdate = date.newInstance(system.today().year(), system.today().month(), system.today().day());
        pMap.Last_Updated_PaymentTypes_Info__c = curdate.adddays(1);
        insert pMap;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock1(true));
        PaymentTypeHelper.getPaymentTypeFromAPI();
        PaymentTypeHelper.getPaymentTypeMapping();
        PaymentTypeHelper.maintainPaymentTypeMapping();
        PaymentTypeHelper.updatePaymentTypeFromAPI();
        Test.stopTest();
        // System.assert(pMap !=null);
    }
    
    @isTest
    public static void NegativeWay(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock1(false));
        //Map<String, Payment_Type__c> pMap = PaymentTypeHelper.getPaymentTypeMapping();
        Payment_Type__c pMap = new Payment_Type__c();
        pMap.TenderCode__c = 'AMEX';
        pMap.TenderCodeDescription__c = 'American Express Card';
        pMap.DocumentNumber__c = '300';
        date curdate = date.newInstance(system.today().year(), system.today().month(), system.today().day());
        pMap.Last_Updated_PaymentTypes_Info__c = curdate.adddays(1);
        insert pMap;
        system.debug('pMap--'+pMap);
       // PaymentTypeHelper.getPaymentTypeFromAPI();
        PaymentTypeHelper.getPaymentTypeMapping();
     //   PaymentTypeHelper.maintainPaymentTypeMapping();
		// PaymentTypeHelper.updatePaymentTypeFromAPI();
        Test.stopTest();
        // System.assert(pMap !=null);
    }
    
    private class Mock1 implements HttpCalloutMock {
        boolean isValid;
        public Mock1(boolean isValid){
            this.isValid = isValid;
        }
        public HTTPResponse respond(HTTPRequest req) {
            Integration_Settings__c pmtTypesConf= Integration_Settings__c.getValues('PaymentTypesAPI');
            system.debug('EP: ' + req.getEndpoint());
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            if (req.getEndpoint().startsWith(pmtTypesConf.End_Point_URL__c) && isValid) {
                res.setBody('[{"TenderCodeDescription":"Desc","TenderCode":"code","DocumentNumber":"1"}]');
                res.setStatusCode(200);
            } else {
                res.setBody('{"salesOrderResult":[{"SalesOrderNumber":0,"DeliveryViaCode":"HD","SalesOrderReferenceId":"b073b150-e8fe-480b-aadb-4e0e7dc2a20a"}],"customerId":"4046433752A"}');
                res.setStatusCode(201);
            }
            res.setStatus('OK');
            return res;
        }
    }
    
}