global class APINpsHelper {
    /**
*   @description: Method to parse JSON string to NpsWrapper
*   @return:      Returns List of NpsWrapper
*/
    public static List<NpsWrapper> parseNpsRequest(String requestString) {
        List<NpsWrapper> NpsOptList = new List<NpsWrapper> ();
        JSONParser parser = JSON.createParser(requestString);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        NpsOptList.add((NpsWrapper)parser.readValueAs(NpsWrapper.class));
                        parser.skipChildren();
                    }
                }
            }
        }
        return NpsOptList;
    }
    
}