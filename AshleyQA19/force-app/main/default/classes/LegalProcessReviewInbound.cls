global class LegalProcessReviewInbound implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String myPlainText= '';
        String Recid;
        String ProcessNodeId;
        
        // Add the email plain text into the local variable 
        myPlainText = email.plainTextBody; 
        myPlainText = myPlainText.toUpperCase();
        system.debug('myPlainText--1-' + myPlainText);
        myPlainText = myPlainText.substringBefore('SENT FROM').trim();
        system.debug('myPlainText---' + myPlainText);
        
        string sub = email.subject;
        system.debug('sub---' + sub);
        system.debug('email---' + email);
        
        Recid = sub.substringAfter('RecId:');
        Recid = Recid.substringBefore('ProcessNodeId:').trim();
        system.debug('Recid---' + Recid);
        
        ProcessNodeId = sub.substringAfter('ProcessNodeId:').trim();
        system.debug('ProcessNodeId---' + ProcessNodeId);
        
        List<Attachment> attachments = new List<Attachment>();
        
        Legal_Review_Request__c LRR = [select Id,Account__c,CreatedBy.Email from Legal_Review_Request__c where Id =: Recid ];
        
        //---- getting the user Id for replied Email ----
        Id userId;
        user usr = [select id,Email from user where Email=:email.FromAddress];
        system.debug('usr--'+usr);
        userId = usr.Id;
        
        //----- getting processes instance Id -----
        string Approvalstatus = '';
        
        List<ProcessInstance> PI = [SELECT Id,TargetObjectId, 
                               		(SELECT Id, ProcessNodeId,StepStatus, actorid,actor.Name,actor.Email FROM StepsAndWorkitems 
                                		where ProcessNodeId=: ProcessNodeId AND StepStatus='Pending' ) 
                               		FROM ProcessInstance where TargetObjectId =: lrr.Id ];
        system.debug('PI---1-'+PI);
        
        for(ProcessInstance pro : PI){
            system.debug('pro----'+pro.StepsAndWorkitems);
            for(ProcessInstanceHistory stp : pro.StepsAndWorkitems){
                system.debug('stp----'+stp);
                if(Test.isRunningTest()){
                    Approvalstatus = 'Pending';
                }else{
                    Approvalstatus = stp.StepStatus;
                }
                system.debug('Actor Name----'+stp.Actor.Name);
            }
        }
        system.debug('Approvalstatus----'+Approvalstatus);
        
        //---- verifying the subject and inserting the reply email in the object -----
        if (String.isNotEmpty(sub) && String.isNotBlank(sub)) {
         	
            //-----Add Inbound Email Message for Legal review object-----
            EmailMessage LRREmailMessage = new EmailMessage();
            LRREmailMessage.ToAddress =  String.join(email.toAddresses, ',');
            system.debug('Email to Address'+email.toAddresses);
            LRREmailMessage.FromAddress = email.FromAddress;
            LRREmailMessage.FromName = email.FromName;
            LRREmailMessage.Subject = sub;
            LRREmailMessage.status = '2';
            LRREmailMessage.HtmlBody = email.htmlBody;
            LRREmailMessage.Incoming= True;
            LRREmailMessage.TextBody = myPlainText;
            LRREmailMessage.RelatedToId = LRR.Id;
            insert LRREmailMessage;
            
            system.debug('LRREmailMessage---' + LRREmailMessage);
            
            //----- verifying for the attachment and inserting to the object------
            if (email.textAttachments != null) {
                system.debug('textAttachments--' + email.textAttachments.size());
                for(Messaging.InboundEmail.textAttachment tAttachment : email.textAttachments) {
                    system.debug('textAttachments--' + tAttachment);
                    Attachment attachment = new Attachment();
                    attachment.Name = tAttachment.fileName;
                    attachment.Body = Blob.valueOf(tAttachment.body);
                    attachment.ParentId = LRR.Id;
                    attachments.add(attachment);
                }
            }
            
            if (email.binaryAttachments !=null) {
                system.debug('binaryAttachments--' + email.binaryAttachments.size());
                for(Messaging.InboundEmail.binaryAttachment bAttachment :email.binaryAttachments) {
                    system.debug('binaryAttachments--' + bAttachment);
                    if (bAttachment.fileName != 'smime.p7s') {
                        Attachment attachment = new attachment();
                        attachment.Name  = bAttachment.fileName;
                        attachment.Body = bAttachment.body;
                        attachment.Parentid = LRR.Id;
                        attachments.add(attachment);
                    }
                }
            }
            system.debug('attach size--'+attachments.size());
            system.debug('attachs----'+attachments);
            
            if (LRREmailMessage.FromAddress != system.label.Legal_Process_Review) {
                
                if(attachments.size() > 0 && Approvalstatus == 'Pending' && (myPlainText.contains('REJECTED') || myPlainText.contains('REJECT') || myPlainText.contains('DENY') ) ){
                    insert attachments;
                    system.debug('reject attachment insert----'+attachments);
                }
                else if(attachments.size() == 0 && Approvalstatus == 'Pending' && (myPlainText.contains('REJECTED') || myPlainText.contains('REJECT') || myPlainText.contains('DENY')) ){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    String toaddress = email.FromAddress;
                    mail.setToAddresses(new String[] {toaddress});
                    mail.setSubject('(Do Not reply): Attachment is mandatory for rejecting Contract');
                    mail.setHtmlBody('Hi '+email.FromName+',<br/><br/> Please provide valid document for rejecting contract, this is not considering as approved/rejected till valid document is provided. <br/><br/>Thanks,<br/>Leagal Team');
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                    system.debug('toaddress----'+toaddress);
                    system.debug('reject mail sent---'+mail);
                }
                else if(attachments.size() >= 0 && (myPlainText.contains('APPROVED') || myPlainText.contains('ACCEPT')) ){
                    insert attachments;
                    system.debug('accept attachments----');
                }
                
            }
        }
        
        //---- verifying the approval status and sending email if status already in Approved/Rejected -----
        if(Approvalstatus == 'Approved' || Approvalstatus == 'Rejected' || Approvalstatus == 'NoResponse'){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String toaddress = email.FromAddress;
            system.debug('toaddress----'+toaddress);
            mail.setToAddresses(new String[] {toaddress});
            mail.setSubject('New Email reply from Legal Request');
            mail.setHtmlBody('This Process is already Completed. To View Click <a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/lightning/r/Legal_Review_Request__c/'+LRR.Id+'/view>here</a> to login Salesforce and view.');
            system.debug('mail---'+mail);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
        
        //---- validating for the approval process & updating the approval status-----
        if( Approvalstatus == 'Pending' && (myPlainText.contains('APPROVED') || myPlainText.contains('ACCEPT')) ){
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            system.debug('entered in if--'+myPlainText);
            req.setAction('Approve');
            req.setComments('Approved request via Email');
            Id workItemId = getWorkItemId(LRR.Id,userId);
            system.debug('workItemId----1-'+workItemId);
            if(workItemId == null){
                LRR.addError('Error Occured');
            }
            else{
                req.setWorkitemId(workItemId);
                // Submit the request for approval
                Approval.ProcessResult LRRresult =  Approval.process(req);
                system.debug('LRRresult--'+LRRresult);
            }
            system.debug('req--'+req);
        }
        
        system.debug('attachments.size--'+attachments.size());
        system.debug('Approvalstatus--'+Approvalstatus);
        system.debug('myPlainText--'+myPlainText);
        
        if( attachments.size() > 0 && Approvalstatus == 'Pending' && (myPlainText.contains('REJECT') || myPlainText.contains('DENY')) ){
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            system.debug('entered in if--'+myPlainText);
            req.setAction('Reject');
            req.setComments('Rejected request via Email');
            Id workItemId = getWorkItemId(LRR.Id,userId);
            system.debug('workItemId----2'+workItemId);
            if(workItemId == null){
                LRR.addError('Error Occured');
            }
            else{
                req.setWorkitemId(workItemId);
                // Submit the request for approval
                Approval.ProcessResult LRRresult =  Approval.process(req);
                system.debug('LRRresult--'+LRRresult);
            }
            system.debug('req--'+req);
        }
        
        
        return result;
    }
    
    /*****Get ProcessInstanceWorkItemId using SOQL***/
    public Id getWorkItemId(Id targetObjectId, Id targetObjectuserId){
        system.debug('targetObjectId--'+targetObjectId);
        system.debug('targetObjectuserId--'+targetObjectuserId);
        Id retVal = null;
        
        for(ProcessInstanceWorkitem workItem : [Select p.Id,p.ActorId,p.OriginalActorId from ProcessInstanceWorkitem p
                                                where p.ProcessInstance.TargetObjectId =: targetObjectId 
                                                And p.ActorId =: targetObjectuserId]) {
            system.debug('workItem---'+workItem);
            retVal = workItem.Id;
            system.debug('retVal---'+retVal);
        }
        
        return retVal;
    }
    
}