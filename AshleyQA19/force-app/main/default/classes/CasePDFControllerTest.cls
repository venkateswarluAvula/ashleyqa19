@isTest
public class CasePDFControllerTest {
    static testMethod void TestData() {
         Test.startTest();
         
         
        //Creating Account
        List<Account> testAccount = TestDataFactory.initializeAccounts(1);
        insert testAccount;
        
        //Creating Contact
        List<Contact> testContact = TestDataFactory.initializeContacts(testAccount[0].Id,1);
        insert testContact;
        
        
        //Address__c testShipping = new Address__c(AccountId__c=testAccount[0].Id;Address_Line_1__c = '123 Test Coverage St', City__c = 'Franklin',StateList__c = 'TN');
        //insert testShipping;
        
        //Creating Case
        List<Case> testCase1 = TestDataFactory.initializeCases(testAccount[0].Id,testContact[0].Id,1);  
        
        Case testCase = new case();
        //testCase.Address__c = testShipping.id;
        testCase.Technician_ServiceReqId__c = String.valueOf(1234);
        testCase.Technician_Company__c = 'test';
        testCase.Technician_Name__c = UserInfo.getUserId();
        testCase.TechnicianNameScheduled__c = String.valueOf(1234);
        testCase.Legacy_Service_Request_ID__c = String.valueOf(1234);
        testCase.Category_Reason_Codes__c = string.valueOf('abcd');
        testCase.Estimated_time_for_stop__c = String.valueOf(1234);
        testCase.Technician_Schedule_Date__c = System.today();
        testCase.Follow_up_Date__c = null;
        testCase.followup_Priority_EstimatedTime__c = String.valueOf(1234);
        testCase.Type_of_Resolution__c = 'test';
        testCase.Company__c = 'test';
        testCase.Legacy_Technician__c = 'test';
        testCase.Tech_Scheduled_Date__c = System.today();
        insert testCase;
        
        List<Case> cas = new List<Case>();
        cas.add(testCase);
        
        Case_Line_Item__c cli = new Case_Line_Item__c();
            //cli.Sales_Order_Number__c = testCase[0].Sales_Order__c;
            cli.Case__c = cas[0].Id;
            cli.Item_SKU__c = '*DELIV-TAX';
            cli.Legacy_Service_Request_Item_ID__c = '102393';
            cli.Delivery_Date__c = System.today();
            cli.Item_Serial_Number__c = '000002870155';
            cli.Part_Order_Tracking_Number__c = '94104';
            
        insert cli;   
        
        ProductLineItem__c pro = new ProductLineItem__c();
        pro.Sales_Order_Number__c = cas[0].Sales_Order__c;
        pro.Case__c = cas[0].Id;
        pro.Item_SKU__c = '*DELIV-TAX';
        pro.Legacy_Service_Request_Item_ID__c = '102393';
        pro.Delivery_Date__c = string.ValueOf('07/15/2018');
        pro.Item_Serial_Number__c = '000002870155';
        pro.Part_Order_Tracking_Number__c = '94104';
        insert pro;   
        
        
       
            //CasePDFController cpc = new CasePDFController();
            //cpc.opendate = string.ValueOf('07/15/2018');
            //String RecId = '5000n00000664KdAAI';
            //CasePDFController.getcaserecords(RecId);
            
            
            //Put Id into the current page Parameters
            //ApexPages.currentPage().getParameters().put('id', RecId);
            
            CasePDFController.getcaserecords(testCase.Id);
            ApexPages.currentPage().getParameters().put('id', testCase.Id);
            
            CasePDFController cpc = new CasePDFController();
            cpc.opendate = 'testString';
            cpc.TechSchDate = 'testSchDte';
            cpc.FolupDate = 'FlupDate';
            
            PageReference myVfPage = Page.CasePDF;
            Test.setCurrentPage(myVfPage);
            
           Test.stopTest();
        
    }
    
    static testMethod void TestData1() {
         Test.startTest();
         
         
        //Creating Account
        List<Account> testAccount = TestDataFactory.initializeAccounts(1);
        insert testAccount;
        
        //Creating Contact
        List<Contact> testContact = TestDataFactory.initializeContacts(testAccount[0].Id,1);
        insert testContact;
        
        
        //Address__c testShipping = new Address__c(AccountId__c=testAccount[0].Id;Address_Line_1__c = '123 Test Coverage St', City__c = 'Franklin',StateList__c = 'TN');
        //insert testShipping;
        
        //Creating Case
        List<Case> testCase1 = TestDataFactory.initializeCases(testAccount[0].Id,testContact[0].Id,1);  
        
        Case testCase = new case();
        //testCase.Address__c = testShipping.id;
        testCase.Technician_ServiceReqId__c = String.valueOf(1234);
        testCase.Technician_Company__c = 'test';
        testCase.Technician_Name__c = UserInfo.getUserId();
        testCase.TechnicianNameScheduled__c = String.valueOf(1234);
        testCase.Legacy_Service_Request_ID__c = String.valueOf(1234);
        testCase.Category_Reason_Codes__c = string.valueOf('abcd');
        testCase.Estimated_time_for_stop__c = String.valueOf(1234);
        testCase.Technician_Schedule_Date__c = System.today();
        testCase.Follow_up_Date__c = System.today();
        testCase.followup_Priority_EstimatedTime__c = String.valueOf(1234);
        testCase.Type_of_Resolution__c = 'test';
        testCase.Company__c = 'test';
        testCase.Legacy_Technician__c = 'test';
        testCase.Tech_Scheduled_Date__c = System.today();
        insert testCase;
        
        List<Case> cas = new List<Case>();
        cas.add(testCase);
        
        Case_Line_Item__c cli = new Case_Line_Item__c();
            //cli.Sales_Order_Number__c = testCase[0].Sales_Order__c;
            cli.Case__c = cas[0].Id;
            cli.Item_SKU__c = '*DELIV-TAX';
            cli.Legacy_Service_Request_Item_ID__c = '102393';
            cli.Delivery_Date__c = System.today();
            cli.Item_Serial_Number__c = '000002870155';
            cli.Part_Order_Tracking_Number__c = '94104';
            
        insert cli;   
        
        ProductLineItem__c pro = new ProductLineItem__c();
        pro.Sales_Order_Number__c = cas[0].Sales_Order__c;
        pro.Case__c = cas[0].Id;
        pro.Item_SKU__c = '*DELIV-TAX';
        pro.Legacy_Service_Request_Item_ID__c = '102393';
        pro.Delivery_Date__c = string.ValueOf('07/15/2018');
        pro.Item_Serial_Number__c = '000002870155';
        pro.Part_Order_Tracking_Number__c = '94104';
        insert pro;   
        
        
       
            //CasePDFController cpc = new CasePDFController();
            //cpc.opendate = string.ValueOf('07/15/2018');
            //String RecId = '5000n00000664KdAAI';
            //CasePDFController.getcaserecords(RecId);
            
            
            //Put Id into the current page Parameters
            //ApexPages.currentPage().getParameters().put('id', RecId);
            
            CasePDFController.getcaserecords(testCase.Id);
            ApexPages.currentPage().getParameters().put('id', testCase.Id);
            
            CasePDFController cpc = new CasePDFController();
            cpc.opendate = 'testString';
            cpc.TechSchDate = 'testSchDte';
            cpc.FolupDate = 'FlupDate';
            
            PageReference myVfPage = Page.CasePDF;
            Test.setCurrentPage(myVfPage);
            
           Test.stopTest();
        
    }
    
    static testMethod void TestData2() {
         Test.startTest();
        
        List<Account> testAccount = TestDataFactory.initializeAccounts(1);
        insert testAccount;
        
        List<Contact> testContact = TestDataFactory.initializeContacts(testAccount[0].Id,1);
        insert testContact;
        
        List<Case> testCase1 = TestDataFactory.initializeCases(testAccount[0].Id,testContact[0].Id,1);  
        
        Case testCase = new case();
        testCase.Technician_ServiceReqId__c = String.valueOf(1234);
        testCase.Technician_Company__c = 'test';
        testCase.Technician_Name__c = UserInfo.getUserId();
        testCase.TechnicianNameScheduled__c = String.valueOf(1234);
        testCase.Legacy_Service_Request_ID__c = String.valueOf(1234);
        testCase.Category_Reason_Codes__c = string.valueOf('abcd');
        testCase.Estimated_time_for_stop__c = String.valueOf(1234);
        testCase.Follow_up_Date__c = null;
        testCase.followup_Priority_EstimatedTime__c = String.valueOf(1234);
        testCase.Type_of_Resolution__c = 'test';
        testCase.Company__c = 'test';
        testCase.Legacy_Technician__c = 'test';
        testCase.Tech_Scheduled_Date__c = System.today();
        insert testCase;
        
        List<Case> cas = new List<Case>();
        cas.add(testCase);
        
        Case_Line_Item__c cli = new Case_Line_Item__c();
            //cli.Sales_Order_Number__c = testCase[0].Sales_Order__c;
            cli.Case__c = cas[0].Id;
            cli.Item_SKU__c = '*DELIV-TAX';
            cli.Legacy_Service_Request_Item_ID__c = '102393';
            cli.Delivery_Date__c = System.today();
            cli.Item_Serial_Number__c = '000002870155';
            cli.Part_Order_Tracking_Number__c = '94104';
            
        insert cli;   
        
        ProductLineItem__c pro = new ProductLineItem__c();
        pro.Sales_Order_Number__c = cas[0].Sales_Order__c;
        pro.Case__c = cas[0].Id;
        pro.Item_SKU__c = '*DELIV-TAX';
        pro.Legacy_Service_Request_Item_ID__c = '102393';
        pro.Delivery_Date__c = string.ValueOf('07/15/2018');
        pro.Item_Serial_Number__c = '000002870155';
        pro.Part_Order_Tracking_Number__c = '94104';
        insert pro;   
        
            CasePDFController.getcaserecords(testCase.Id);
            ApexPages.currentPage().getParameters().put('id', testCase.Id);
            
            CasePDFController cpc = new CasePDFController();
            cpc.opendate = 'testString';
            cpc.TechSchDate = 'testSchDte';
            cpc.FolupDate = 'FlupDate';
            
            PageReference myVfPage = Page.CasePDF;
            Test.setCurrentPage(myVfPage);
            
           Test.stopTest();
        
    }
    

  }