@RestResource(urlMapping='/ManageContacts/*')
global class API_ManageContacts {
    
    global class ContactWrapper {
        public Contact[] Contacts;
        public String status;
        public String message;
        public ContactWrapper(){
            Contacts = new List<Contact>();
        }
    }
    global class ManageContactWrapper {
        public Contact[] Contacts;
        public String SFDCAccountId;
        public String Id;
        public String Subject;
        public String FirstName;
        public String LastName;
        public String Name;
        public String Email;
        public String Email2;
        public String Phone;
        public String Phone2;
        public String Phone3;
        public String Type;
        public String Subtype;
        public String Mobile;
        public String Description;
        public String PhoneType;
        public String Phone2Type;
        public String Phone3Type;
        public String PrimaryLanguage;
        public String TypeofCustomer;
        
        public ManageContactWrapper(){
            Contacts = new List<Contact>();
        }
    }
    // Create or Update/Edit GuestNotes
    @HttpPost
    global static ContactWrapper doPost() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        String JSONString = req.requestBody.toString();
        Map<String, Object> Someval = (Map<String, Object>)JSON.deserializeUntyped(JSONString);
        List<Map<String, Object>> data = new List<Map<String, Object>>();
        for (Object instance : (List<Object>)Someval.get('Contacts')){
            data.add((Map<String, Object>)instance);
            system.debug('data-->'+data);
        } 
        system.debug('RestContext.reque-->'+req);
        ContactWrapper responseBody = new ContactWrapper();
        Boolean InsertFlag = false;
        List<ManageContactWrapper> ConsList = new List<ManageContactWrapper>();
        Map<String, ManageContactWrapper> ActccountMapJSON = new Map<String, ManageContactWrapper>();
        for(integer i=0; i<data.size(); i++){
            String Someval3 = JSON.serialize(data[i]);
            system.debug('Someval3-->'+Someval3);
            ManageContactWrapper Someval4 = (ManageContactWrapper)JSON.deserialize(Someval3,ManageContactWrapper.class);
            ConsList.add(Someval4);
        }
        system.debug('ContactsList-->'+ConsList);
        List<Contact> ConList = new List<Contact>();
        for(ManageContactWrapper mcw : ConsList){
            List<Contact> con = new List<Contact>();
            Boolean checkAccountOnInsertion = false;
            con = CreateContact(mcw);
            System.debug('Contacts-->'+con);
            if(con.size() > 0){
                for(integer i = 0; i < con.size(); i++){
                        ConList.add(con[i]);
                    }
                }
        }
        if(ConList.size()>0){
            upsert ConList;
            InsertFlag = true;
        }
        Boolean IFlag = false;
        if(InsertFlag == TRUE){
            IFlag = true;
        } 
        if(IFlag == true){
            responseBody.Contacts = ConList;
            responseBody.status = 'Success';
            responseBody.message = 'Contacts Inserted/Updated successfully..';
            return responseBody; 
        } 
        else{
            responseBody.status = 'Success';
            responseBody.message = 'Insertion or Updation Failed..';
            return responseBody; 
        }
    }
     // Creating or Updaing Contacts.
    public static List<Contact> CreateContact(ManageContactWrapper wrapup){
        List<Contact> listcon = new List<Contact>();
        Contact con = New Contact();
        con.AccountId  = Label.Ashley_Enterprise_Employee_List; 
        con.Email = wrapup.Email;
        con.Email_2__c = wrapup.Email2;
        con.FirstName = wrapup.FirstName;
        con.LastName = wrapup.LastName;
        con.Phone = wrapup.Phone;
        con.Phone_2__c = wrapup.Phone2;
        con.Phone_3__c = wrapup.Phone3;
        con.Phone_Type__c = wrapup.PhoneType;
        con.Phone_2_Type__c = wrapup.Phone2Type;
        con.Phone_3_Type__c = wrapup.Phone3Type;
        con.Type_of_Customer__c = wrapup.TypeofCustomer;
        con.Primary_Language__c = wrapup.PrimaryLanguage;
        if(wrapup.Id != null && wrapup.Id != ''){
            con.Id = wrapup.Id;
            Update con;
        }else{
            Insert con;
        }
        listcon.add(con);
        return listcon;
    }
}