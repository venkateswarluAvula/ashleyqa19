global class GetProfileObjectPermission {
    @AuraEnabled
    public static void getTrackingNumber(){
        String csvColumnHeader = '';
        List<EntityDefinition> sObjectList = new List<EntityDefinition>();
        set<string> SL = new set<string>();
        sObjectList = [SELECT  QualifiedApiName FROM EntityDefinition order by QualifiedApiName];
        for(EntityDefinition e : sObjectList){
            SL.add(e.QualifiedApiName);
        }
        Map<EntityDefinition, List<PermissionSet>> ProfileObjectPermission = new Map<EntityDefinition, List<PermissionSet>>();
        List<PermissionSet> PermissionList = new List<PermissionSet>();
        PermissionList = [SELECT ID,PermissionSet.Profile.Name FROM PermissionSet WHERE Id IN 
                              (SELECT ParentId FROM ObjectPermissions WHERE SObjectType IN :SL)];
            
        for(EntityDefinition ed : sObjectList){
            if(PermissionList.size()<0){
                ProfileObjectPermission.put(ed, PermissionList);
            }
        }
        String header = '--sObject--' + '--Profiles--'+'<br/>';
        String colRow ;
        for (EntityDefinition ed : ProfileObjectPermission.keySet()) {
            List<PermissionSet> ProfilesLst = ProfileObjectPermission.get(ed);
            for(PermissionSet ps :ProfilesLst){
                colRow = colRow + ed.QualifiedApiName + '-->' + ps + '<br/>';
            }
        }
        String messageBody = '<html><body>' + header + colRow+'<br/><br/></body></html>';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'ssaha@ashleyfurniture.com','kbharti@ashleyfurniture.com'});
      //  mail.setToAddresses(new String[] {'ssaha@ashleyfurniture.com','sudeshna.saha@kcspl.co.in'});
        mail.setReplyTo('ssaha@ashleyfurniture.com');
        mail.setSenderDisplayName('sObject---Profile');
        mail.setSubject('sObject---Profile');
        mail.setHtmlBody(messageBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}