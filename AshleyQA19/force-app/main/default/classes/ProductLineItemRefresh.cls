global class ProductLineItemRefresh implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
    global Integer PLIProccessed =0;
    global Integer PLIProccessedSuccess = 0 ;
    global Integer PLISkipped = 0 ;
    global List<ProductLineItem__c> listtoupdate = new List<ProductLineItem__c>();
    global List<Market_Configuration__mdt> MarketConfig = new List<Market_Configuration__mdt>();
    global List<Group> queueList = new list<Group>();
    global List<Group> SkippedOwnerQueue = new List<Group>();
    global set<ID> SkippedIds = new set<ID>();
    global Database.QueryLocator start(Database.BatchableContext bc){
        List<ProductLineItem__c> PLIwithTN = [SELECT Id  FROM ProductLineItem__c 
                                         WHERE (Case__r.Status != 'Closed' AND Case__r.Status != 'Stale' AND Case__r.Status != 'Closed in Salesforce' AND Case__r.Status != 'Paid and Closed' AND Case__r.Status != 'Parts Order No Tech Required' AND Case__r.Status != 'Parts Order Not Tech Required') AND (Part_Order_Tracking_Number__c != ''OR Part_Order_Tracking_Number__c != null ) 
                 						 AND Fulfiller_ID__c != null AND Fulfiller_ID__c != '' 
                 						 AND Ashley_Direct_Link_ID__c != null AND Ashley_Direct_Link_ID__c != '' LIMIT 50000];
        PLISkipped = PLIwithTN.size();
        MarketConfig = [SELECT Fulfiller_Id__c,Id,Label,MasterLabel,QualifiedApiName,Routing_Queue_Name__c FROM Market_Configuration__mdt];
        queueList = [SELECT DeveloperName,Id FROM Group WHERE DeveloperName = 'East_Tech_Schedule' OR DeveloperName = 'West_Tech_Schedule' OR DeveloperName = 'Parts_Ordered_Cases'];
        SkippedOwnerQueue = [SELECT DeveloperName,Id FROM Group WHERE DeveloperName = 'Compliance' OR DeveloperName = 'Warranty_Review_Team' OR DeveloperName = 'FPP' OR DeveloperName = 'SWF_FPP'];
        for(Integer i=0; i < SkippedOwnerQueue.size(); i++){
            SkippedIds.add(SkippedOwnerQueue[i].Id);
        }
        return Database.getQueryLocator([SELECT Id,Part_Order_Number__c,Fulfiller_ID__c,Part_Order_Num__c,Ashley_Direct_Link_ID__c,
                                         Case__r.Id, Case__r.Status, Case__r.Technician_Schedule_Date__c, Case__r.Tech_Scheduled_Date__c FROM ProductLineItem__c 
                                         WHERE (Case__r.Status != 'Closed' AND Case__r.Status != 'Stale' AND Case__r.Status != 'Closed in Salesforce' AND Case__r.Status != 'Paid and Closed' AND Case__r.Status != 'Parts Order No Tech Required' AND Case__r.Status != 'Parts Order Not Tech Required') AND (Part_Order_Tracking_Number__c = ''OR Part_Order_Tracking_Number__c = null ) 
                 						 AND Fulfiller_ID__c != null AND Fulfiller_ID__c != '' 
                 						 AND Ashley_Direct_Link_ID__c != null AND Ashley_Direct_Link_ID__c != ''
                                         AND LastModifiedDate = LAST_N_DAYS:30
                 						 ]);  
    }
 	global void execute(Database.BatchableContext bc, List<ProductLineItem__c> PLIlist){
        try{
            PLIProccessed = PLIProccessed + PLIlist.size();
            List<ProductLineItem__c> listtoupdateBatch = new List<ProductLineItem__c>();
            Set<ID> CaseIDWithTech = new Set<ID>();
            Set<ID> CaseIDWithRPOandWithoutTech = new Set<ID>();
            List<Case> Cases = new List<Case>();
            List<Case> CasesToUpdate = new List<Case>();
            ServiceReplacementPartsAuthorization replParts= new ServiceReplacementPartsAuthorization();
            System.debug('PLIlist-->'+PLIlist.size());
            List<List<ProductLineItem__c>> PLIListOfList = new List<List<ProductLineItem__c>>();
            for(ProductLineItem__c PLI : PLIlist){
                if(PLI.Fulfiller_ID__c != null && PLI.Fulfiller_ID__c != '' && PLI.Ashley_Direct_Link_ID__c != null && PLI.Ashley_Direct_Link_ID__c != ''){
                    ProductLineItem__c updatePLI = new ProductLineItem__c();
                    updatePLI = ProductLineItemAutoRefresh.getTrackingNumber(PLI,replParts.accessTokenData());
                    System.debug('updatePLI-->' +updatePLI);
                    if(updatePLI != null){
                        if(updatePLI.Part_Order_Number__c != null && updatePLI.Part_Order_Number__c != ''){
                            boolean techdate = false;
                            system.debug('Technician_Schedule_Date__c---'+PLI.Case__r.Technician_Schedule_Date__c);
                            if(PLI.Case__r.Technician_Schedule_Date__c > Date.today() && PLI.Case__r.Technician_Schedule_Date__c != null ){
                                techdate = true;
                             }else if(PLI.Case__r.Tech_Scheduled_Date__c > Date.today() && PLI.Case__r.Tech_Scheduled_Date__c != null){
                                techdate = true;
                            }
                            if(techdate){
                                CaseIDWithTech.add(PLI.Case__r.ID);
                            }
                            else{
                                CaseIDWithRPOandWithoutTech.add(PLI.Case__r.ID);
                            }
                        }
                        PLIProccessedSuccess = PLIProccessedSuccess + 1;
                        listtoupdateBatch.add(updatePLI);
                    }
                }
            }
            if(CaseIDWithTech.Size() > 0 || CaseIDWithRPOandWithoutTech.Size() > 0 ){
                Cases = [Select Id,Legacy_Account_Ship_To__c,status,ownerId,type,Sub_Type__c,Resolution_Notes__c,LastModifiedDate,Type_of_Resolution__c,Technician_Schedule_Date__c,Tech_Scheduled_Date__c From Case where (ID IN :CaseIDWithTech OR ID IN :CaseIDWithRPOandWithoutTech) AND (ownerId NOT IN :SkippedIds) AND (Sub_Type__c != 'Home Damage' AND Sub_Type__c != 'FPP')];
            }
            string queueName;
            for(Case c:Cases){
                System.debug('c.ID-->'+c.ID);
                System.debug('c.owner-->'+c.OwnerId);
                if(CaseIDWithTech.Contains(c.ID)){
                    System.debug('c.ID-->'+c.Id);
                    if(c.Legacy_Account_Ship_To__c != '' && c.Legacy_Account_Ship_To__c != null){
                        for(Market_Configuration__mdt mcm : MarketConfig){
                            if(mcm.Fulfiller_Id__c == c.Legacy_Account_Ship_To__c){
                                queueName = mcm.Routing_Queue_Name__c;
                                for(Group owID : queueList){
                                    if(owID.DeveloperName == queueName){
                                        if(c.OwnerId != owID.Id){
                                            c.OwnerId = owID.Id;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    queueName = 'Parts_Ordered_Cases';
                    for(Group owID : queueList){
                        System.debug('owID-->'+owID.Id);
                        if(owID.DeveloperName == queueName){
                            if(c.OwnerId != owID.Id){
                                c.OwnerId = owID.Id;
                            }
                        }
                    }
                }
                CasesToUpdate.add(c);
            }
            System.debug('PLIProccessedSuccess-->'+PLIProccessedSuccess);
            System.debug('listtoupdateBatch-->'+listtoupdateBatch);
            System.debug('CasesToUpdate-->'+CasesToUpdate);
            if(listtoupdateBatch.size() > 0){
                List<Database.SaveResult> results = Database.update(listtoupdateBatch, false);
                Database.update(CasesToUpdate, false);
            //	Database.update(listtoupdate, false);
            }
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR,'Error :'+e.getMessage() + 'at line Number' + e.getLineNumber());   
        }
    }
    global void finish(Database.BatchableContext bc)
    {
        AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors,
                          a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById,
                          a.CompletedDate, a.CreatedDate From AsyncApexJob a WHERE id = :BC.getJobId()];
        System.debug('a--->'+a.CreatedById);
        String messageBody = '<html><body>' + 'Auto-Update PLI Tracking Batch Process Completed'+'<br/>'+'Total PLI processed : '+ PLIProccessed +'<br/>'+'Total PLI skipped with Tracking Number : '+ PLISkipped +'<br/>'+'Total Successful PLI updates : '+ PLIProccessedSuccess +'<br/>'+'Start Time : '+a.CreatedDate+'<br/>'+'Finish Time : '+a.CompletedDate+'<br/>'+'</body></html>';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'ssaha@ashleyfurniture.com','awashburn@ashleyfurniture.com'});
    //    mail.setToAddresses(new String[] {'ssaha@ashleyfurniture.com','sudeshna.saha@kcspl.co.in'});
        mail.setReplyTo('ssaha@ashleyfurniture.com');
        mail.setSenderDisplayName('Auto-Update PLI Tracking Status Batch Processing Production');
        mail.setSubject('Auto-Update PLI Tracking Batch Process Completed Production');
        mail.setHtmlBody(messageBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}