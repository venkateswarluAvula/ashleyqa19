public class ProductReviewWrapper {
    @AuraEnabled
    public integer id{get;set;}
    
    @AuraEnabled
    public String ProductId{get;set;}
    @AuraEnabled
    public Integer rating{get;set;}
    @AuraEnabled
    public String text{get;set;}
    @AuraEnabled
    public List<ProductReviewcatalogIt> catalogItems{get;set;}
    @AuraEnabled
    public ProductUserReviewWrap user {get;set;} 
    
}