public class techschedulingcancellation {
    @AuraEnabled
    public static string getRelatedServiceRequestsforcancelling(Id recordId) {
        List<case> selectedCaseList = [select id, Technician_Schedule_Date__c, Technician_ServiceReqId__c, Status, Tech_Scheduled_Date__c, Legacy_Service_Request_ID__c from case where id=:recordId];
        system.debug('selectedCaseList: ' + selectedCaseList);

        string confirmMsg;
		if (selectedCaseList.size() > 0) {
			case selectedCase = selectedCaseList[0];

			string reqId;
			if (selectedCase.Legacy_Service_Request_ID__c != null) {
				reqId = selectedCase.Legacy_Service_Request_ID__c;
			} else if (selectedCase.Technician_ServiceReqId__c != null) {
				reqId = selectedCase.Technician_ServiceReqId__c;
			}
			system.debug('reqId: ' + reqId);

	        if (reqId != null) {
	        	//user is allowed to unschedule technician
	        	date scheduledDate;
	        	if (selectedCase.Technician_Schedule_Date__c != null) {
	        		scheduledDate = selectedCase.Technician_Schedule_Date__c;
	        	} else if ((selectedCase.Tech_Scheduled_Date__c != null) && (selectedCase.Tech_Scheduled_Date__c > date.newInstance(1900, 01, 01))) {
	        		scheduledDate = selectedCase.Tech_Scheduled_Date__c;
	        	}
	        	system.debug('scheduledDate: ' + scheduledDate);

	        	if (scheduledDate != null) {
		        	if (scheduledDate >= system.today()) {
		        		//Allow to unschedule
		        		confirmMsg = 'Allow:'+reqId;
		        	} else {
		        		//Show pop up confirmation before unschedule
		        		confirmMsg = 'Confirm:'+reqId;
		        	}
	        	} else {
	        		confirmMsg = 'UnScheduled:'+reqId;
	        	}

				if (string.isNotBlank(selectedCase.Status) && CaseHelper.caseIsClosed(selectedCase.Status)) {
                    confirmMsg = 'Closed:'+reqId;
				}
	        }
		}

        system.debug('confirmMsg: ' + confirmMsg);
        return confirmMsg;
    }

    @AuraEnabled
    public static Case getCaseObj(Id recordId) {
        Case caseObj = [select id, CaseNumber, Sales_Order__c, Legacy_Account_Ship_To__c, Address__c, Technician_ServiceReqId__c, Technician_Schedule_Date__c,
						Technician_Company__c, TechnicianNameScheduled__c, followup_Priority_EstimatedTime__c, Technician_Address__c, Status, Legacy_Service_Request_ID__c,
                        Legacy_Assignee__c, Company__c, Technician_Id__c, Legacy_Technician__c, Estimated_time_for_stop__c, Tech_Scheduled_Date__c, Follow_up_Date__c
                        from Case Where Id = : recordId];
        return caseObj;
    }

	/*
    public static string getfulfillerId(string extId){
        system.debug('Ext Id Full: '+ extId);
        SalesOrder__x salesOrderObj = new SalesOrder__x();
        if(Test.isRunningTest()){
            salesOrderObj = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                              ExternalId = '204258:0012900000GRH2LAAX');
            
        }else{
            salesOrderObj = [SELECT ExternalId, fulfillerID__c, Id, phhProfitcenter__c, phhCustomerID__c 
                             FROM SalesOrder__x 
                             WHERE ExternalId =: extId];
        }
        system.debug('SO Obj Full fill: '+ salesOrderObj);
        return salesOrderObj.fulfillerID__c;
    }
    */

    @AuraEnabled
    public static string getApiResponse(Id recordId, boolean apiCall, Id adrId) {
    	string returnStr;
    	boolean unscheduleError = false;
        Case caseObj = getCaseObj(recordId); // getZipCode(caseObj.Sales_Order__c)
        if (apiCall) {
			User objUser = [Select Id, CommunityNickname, timezonesidkey from User where Id=:UserInfo.getUserId()];
			string techSchUserName = '';
			if (string.isNotBlank(objUser.CommunityNickname)) {
				techSchUserName = objUser.CommunityNickname;
			}

            string techScheduleId;
            if(string.isNotBlank(caseObj.Legacy_Service_Request_ID__c)){
                techScheduleId = caseObj.Legacy_Service_Request_ID__c;
            }
            else if(string.isNotBlank(caseObj.Technician_ServiceReqId__c)){
                techScheduleId = caseObj.Technician_ServiceReqId__c;
            }

			//need to get case fulfiller id based on account ship to or sales order
			string marketFulfillerId = CaseHelper.getCaseFulfillerId(caseObj.Legacy_Account_Ship_To__c, caseObj.Sales_Order__c);

			//if the fulfiller id is not blank then go ahead with the call out else skip
			if (string.isNotBlank(marketFulfillerId)) {
				//string soFulfillerId = getfulfillerId(caseObj.Sales_Order__c);
	        	String httpReqVar = system.label.TechSchedulingEndPoint + marketFulfillerId + '/customer-service/service-technician-unschedule?userId=1&requestId=' + techScheduleId + '&userName=' + techSchUserName;
	        	httpReqVar = httpReqVar.trim();

		        Http http = new http();
		        Httprequest req = new HttpRequest();
		        req.setHeader('apikey', system.label.TechSchedulingApiKey);
		        req.setHeader('Accept', 'application/json');
		        req.setEndpoint(httpReqVar);
		        req.setTimeOut(120000);
		        req.setMethod('GET');

				CaseHelper.CalloutResponse res = CaseHelper.returnCalloutResponse(req, 'Technician Unschedule call', 'techschedulingcancellation', 'getApiResponse', httpReqVar);
	        	system.debug('****** GET Response Status: ' + res.HttpRespObj.getStatusCode());
	        	system.debug('****** GET Response: ' + res.HttpRespObj.getBody());

		        if (res.HttpRespObj.getStatusCode() == 200) {
		        	// technician unscheduled successfully in HOMES service request
		        	returnStr = 'Successfully Unscheduled';

					//string section, Id caseId, integer RequestId, string fulfillerId, string scheduledDate, integer totLineItem, string oldAdr, string newAdr
					map<string, string> strMap = new map<string, string>();
					map<string, integer> intMap = new map<string, integer>();
					map<string, id> idMap = new map<string, id>();

					strMap.put('section', 'unschedule');
					idMap.put('caseId', recordId);
					intMap.put('RequestId', integer.valueOf(techScheduleId));
					strMap.put('fulfillerId', marketFulfillerId);

					CaseHelper.servReqCommentInHomes(strMap, intMap, idMap);

					/*
					User objUser = [Select Id, CommunityNickname, timezonesidkey from User where Id=:UserInfo.getUserId()];
					datetime dtDateTime = system.now();
					string strDateTime = dtDateTime.format('MM/dd/yyyy HH:mm:ss', objUser.timezonesidkey );
					system.debug(strDateTime);

			        //create a comment on unScheduling a technician
			        JSONGenerator comGen = JSON.createGenerator(true);

					//JSON start
			        comGen.writeStartObject();
			        comGen.writeNumberField('RequestId', integer.valueOf(techScheduleId));
			        comGen.writeFieldName('CommentText');
					comGen.writeStartArray();
					comGen.writeString('Technician Unscheduled from Salesforce Case: ' + caseObj.CaseNumber + ' by ' + objUser.CommunityNickname + ' on ' + strDateTime + ' (' + objUser.timezonesidkey + ')');
			        comGen.writeEndArray();
			        comGen.writeNumberField('CreatedUserID', 0);
			        comGen.writeEndObject();
			        //JSON end

			        String jsoncomString = comGen.getAsString();

					//Comment callout starts
			        HttpRequest comReq = new HttpRequest();
			        comReq.setEndpoint(system.label.TechSchedulingEndPoint + marketFulfillerId +'/customer-service/service-requests/comments');
			        comReq.setHeader('apikey', system.label.TechSchedulingApiKey);
			        comReq.setMethod('POST');
			        comReq.setBody(jsoncomString);
			        comReq.setTimeOut(120000);
			        comReq.setHeader('Content-Type' ,'application/json');
			        system.debug('****** COM POST End Point: ' + comReq.getEndpoint());
		    	    system.debug('****** COM POST body: ' + comReq.getBody());

					HttpResponse comRes = CaseHelper.callApi(comReq, 'Comments call', 'techschedulingcancellation', 'getApiResponse', jsoncomString);
					//Comment callout ends
			        system.debug('****** COM POST Response Status: ' + comRes.getStatusCode());
		    	    system.debug('****** COM POST Response: ' + comRes.getBody());
		    	    */
		        } else if (res.HttpRespObj.getStatusCode() == 400) {
		        	//Either service request is closed or technician is not scheduled in HOMES for the given service request
		        	// "Message": "Invalid Request Id or Technician not Scheduled."
		        	unscheduleError = true;
		        	returnStr = 'We were unable to unschedule this technician because of an error in HOMES. ';
		        	returnStr += 'The most likely cause of the error is that the corresponding Service Request: ' + techScheduleId + ' is closed in HOMES. ';
		        	returnStr += 'Please check HOMES and verify (and re-open the Service Request if necessary) then try again.';
		        } else {
		        	returnStr = 'Error while unschedule this technician, please contact your administrator.';
		        }

				if (res.InsErrorLogWrapList.size() > 0) {
					CaseHelper.insertError(res.InsErrorLogWrapList);
				}
			} else {
				//since fulfiller id is blank, considering there is no related service request in HOMES and unscheduling in salesforce
				returnStr = 'Successfully Unscheduled in Salesforce';
			}

        } else {
        	returnStr = 'Successfully Unscheduled in Salesforce';
        }

		if (!unscheduleError) {
			caseObj.Legacy_Assignee__c = null;
			caseObj.Company__c = null;
            caseObj.Technician_Id__c = 0;
			caseObj.Legacy_Technician__c = null;
            caseObj.Estimated_time_for_stop__c = null;
            caseObj.Tech_Scheduled_Date__c = null;
            caseObj.Follow_up_Date__c = null;

	        caseObj.Technician_Schedule_Date__c = null;
	        caseObj.Technician_Company__c = '';
	        caseObj.TechnicianNameScheduled__c = '';
	        caseObj.followup_Priority_EstimatedTime__c = '';
	        caseObj.Technician_Address__c = '';
	        if (adrId != null) {
	        	caseObj.Address__c = adrId;
	        }

			try {
	        	Database.SaveResult sr = Database.update(caseObj, false);
	        	CaseHelper.databaseErrorLog(sr, 'Case update try Error', 'techschedulingcancellation', 'getApiResponse', JSON.serialize(caseObj));
			} catch (DmlException de) {
	        	CaseHelper.dmlExceptionError(de, 'Case update catch Error', 'techschedulingcancellation', 'getApiResponse', JSON.serialize(caseObj));
			}
		}
        return returnStr;
    }
}