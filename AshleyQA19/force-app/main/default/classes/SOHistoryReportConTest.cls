@isTest
private class SOHistoryReportConTest { 

    public static testMethod void testdata() {
        
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                     ExternalId = '17331400:001q000000raDkvAAE',  
                                                     phhProfitcenter__c = 1234567,
                                                     Phhcustomerid__c = '784584585',
                                                     phhSalesOrder__c = '88845758',
                                                     phhStoreID__c = '133'
                                                    );
        
        SalesOrderDAO.mockedSalesOrders.add(salesOrder);
        system.debug('order--2----' + salesOrder.Id);
        SalesOrder__x salesOrderObj = SalesOrderDAO.getOrderById(salesOrder.Id);
        SalesOrderItem__x salesOrderItem = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE', 
                                                                 phdShipZip__c = '30548');
        SalesOrderDAO.mockedSalesOrderLineItems.add(salesOrderItem);
        SalesOrderItem__x salesOrderItemObj = SalesOrderDAO.getOrderLineItemByExternalId(salesOrderItem.ExternalId);
        
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('SOHistoryReportCon1');
        
        mock.setStatusCode(200);
        
        mock.setHeader('Content-Type', 'application/json');
        
                        
        Account acc = new Account();
        acc.name = 'Acc Name';
        insert acc;
        
        Test.setCurrentPage(Page.SOHistoryReportPDF);
        ApexPages.currentPage().getParameters().put('Id',acc.Id);
        ApexPages.currentPage().getParameters().put('SO',salesOrder.Id);
        ApexPages.currentPage().getParameters().put('dwl','download');
        
        /*
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('SOHistoryReportCon1');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        */
        /*SingleRequestMock mock = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"items": [{"Name": "sForceTest1"}]}',
                                                 null); */
        //String jsonStr2 = '{{ "Storeid": "133", "ProfitCenter": "23", "SaleOrder": "200462400","CustomerId": "8989898989I ","SalesForceId": "001q0000014SzryAAC"},{ "Storeid": "133", "ProfitCenter": "23", "SaleOrder": "200462400","CustomerId": "8989898989I ","SalesForceId": "001q0000014SzryAAC"}}';
        //SingleRequestMock mock = new SingleRequestMock(200,'Complete', jsonStr2, null); 
        Test.startTest();      
        
        Test.setMock(HttpCalloutMock.class, mock);
        //Test.setMock(HttpCalloutMock.class, mock); 
        SOHistoryReportCon sohr = new SOHistoryReportCon();
        
        
        SOHistoryReportCon.inneritemswrap inwrap = new SOHistoryReportCon.inneritemswrap();
        SOHistoryReportCon.outeritemswrap outwrap = new SOHistoryReportCon.outeritemswrap();
        
        
        Test.stopTest(); 
    }
    
}