global class ADSEmailControllerInbound implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {

        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String myPlainText= '';
        // Add the email plain text into the local variable 
        myPlainText = email.plainTextBody;        
        string sub = email.subject;
        system.debug('myPlainText---' + myPlainText);
        system.debug('sub---' + sub);

        string [] bb = new string[]{};
        String Recid;

        if (String.isNotEmpty(sub) && String.isNotBlank(sub)) {
            Recid = sub.substringAfter('RecId:');
            Recid = Recid.substringBefore('AccId:').trim();
            system.debug('Recid---' + Recid +'--'+Recid.Length());

            Legal_Review_Request__c LRR = [select Id,Account__c,CreatedById,CreatedBy.Name,CreatedBy.Email from Legal_Review_Request__c where Id =: Recid ];

            system.debug('LRR---' + LRR);
            // For storing Relation Id
            string accid = LRR.Account__c;

            //Add Inbound Email Message for Case
            EmailMessage LRREmailMessage = new EmailMessage();
            LRREmailMessage.ToAddress =  String.join(email.toAddresses, ',');
            system.debug('Email to Address'+email.toAddresses);
            LRREmailMessage.FromAddress = email.FromAddress;
            LRREmailMessage.FromName = email.FromName;
            LRREmailMessage.Subject = sub;
            LRREmailMessage.status = '2';
            LRREmailMessage.HtmlBody = email.htmlBody;
            LRREmailMessage.Incoming= True;
            LRREmailMessage.TextBody = myPlainText;
            LRREmailMessage.RelatedToId = LRR.Id;
            //LRREmailMessage.ParentId = LRR.Id;

            insert LRREmailMessage;
            system.debug('LRREmailMessage---' + LRREmailMessage);
            system.debug('LRREmailMessage.FromAddress---' + LRREmailMessage.FromAddress );

            List<Attachment> attachments = new List<Attachment>();

            if (email.textAttachments != null) {
                system.debug('textAttachments--' + email.textAttachments.size());
                for(Messaging.InboundEmail.textAttachment tAttachment : email.textAttachments) {
                    system.debug('textAttachments--' + tAttachment);
                    Attachment attachment = new Attachment();
                    attachment.Name = tAttachment.fileName;
                    attachment.Body = Blob.valueOf(tAttachment.body);
                    attachment.ParentId = LRR.Id;
                    attachments.add(attachment);
                }    
                system.debug('attachs1--'+attachments);
            }

            if (email.binaryAttachments !=null) {
                system.debug('binaryAttachments--' + email.binaryAttachments.size());
                for(Messaging.InboundEmail.binaryAttachment bAttachment :email.binaryAttachments) {
                    system.debug('binaryAttachments--' + bAttachment);
                    if (bAttachment.fileName != 'smime.p7s') {
                        Attachment attachment = new attachment();
                        attachment.Name  = bAttachment.fileName;
                        attachment.Body = bAttachment.body;
                        attachment.Parentid = LRR.Id;
                        attachments.add(attachment);
                    }
                }
                system.debug('attachs2--'+attachments);
            }
            system.debug('from Label--'+system.label.ADS_From_Email_Address);

            if (LRREmailMessage.FromAddress != system.label.ADS_From_Email_Address) {
                //if an email is send from salesforce then no need to attach any attachment or send a notification to the requester
	            if (attachments.size() > 0) {
	                system.debug('attach--'+attachments.size());
    	            insert attachments;
        	    }

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String toaddress =  LRR.CreatedBy.Email;
                system.debug('toaddress----'+toaddress);
                //string toaddress = 'karthik.valakonda@d4insight.com';
                mail.setToAddresses(new String[] {toaddress});
                mail.setSubject('New Email reply from Legal Request');
                mail.setHtmlBody('There is a new email reply from Legal. Click <a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/lightning/r/Legal_Review_Request__c/'+LRR.Id+'/view>here</a> to login to Salesforce to view the reply.');
                system.debug('mail---'+mail);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            }
        }
        return result;
    }
}