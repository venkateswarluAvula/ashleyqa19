/****** v1 | Description:Operation on Shopping Cart Line Item  | 1/3/2018 | JoJo Zhao */
public class ShoppingCartLineItemCmpCtrl {
    public static final String DISCOUNT_PENDING_APPROVAL='Discount Pending Approval';
    public static final String NOT_APPLICABLE='Not Applicable';
    public static final String ATP_API_SETTING_NAME = 'AtpAPI';
    public static final String PATH_SETTING_NAME = 'ProductAPISetting';
    public class ATPApiException extends Exception {}
    /**
* @description <get one product detail>                                                        
* @param <productSKUId : product sku value>
* @return <ProductWrapper>
**/
    @AuraEnabled
    public static ProductWrapper getProductDetail(String productSKUId, String lineItemId) {  
        ProductWrapper pw = ProductDetailCmpCtrl.getProductDetail(productSKUId);
        //REQ-438 to save productTitle to lineItem
        if(lineItemId!=null && pw!=null){
            Shopping_cart_line_item__c updateItem = new Shopping_cart_line_item__c(Id=lineItemId);
            updateItem.Product_Title__c = pw.productTitle;
            update updateItem;
        }
        return pw;
    }
    
    /**
* @description <get product list of certain searchKey>                                                        
* @param <searchKey: searchKey>
* @return <A list of ProductWrapper>
**/
    @AuraEnabled
    public static String getProductListSearch(String searchKey, Integer currentIndex, Integer rows, String filterStr, String lineItemId) {
        return ProductListCmpCtrl.getProductListSearch(searchKey, currentIndex, rows, filterStr);
    }   
    @AuraEnabled
    public static void saveProductTitle(String productTitle, String lineItemId) {
        Shopping_cart_line_item__c updateItem = new Shopping_cart_line_item__c(Id=lineItemId);
        updateItem.Product_Title__c = productTitle;
        update updateItem;
    }    
    
    @AuraEnabled
    public static Shopping_cart_line_item__c getLineItemDetail(String lineItemId) {  
        //REQ-438,  add Product_Title__c for Discount Modal product name column show for items list
        Shopping_cart_line_item__c lineItem = [Select Id, WarrantySku__c,Product_Title__c,Product_SKU__c, Discount__c,Discount_Price__c,DiscountType__c,
                                               Flat_Discounted_Amount__c,Flat_Discounted_Amount_On_Total__c,Last_Price__c,List_Price__c,
                                               List_Price_on_Total__c,Average_Cost__c, Quantity__c, As_Is__c,isCallItem__c,
                                               Opportunity__c,Delivery_Mode__c,	DeliveryType__c,Discount_Reason_Code__c,Discount_Status__c,Extended_Price__c,ItemType__c,DeliveryDate__c,
                                               Discount_Approver__c,Discount_Approver__r.Name,
                                               // DEF-0780 - ATC Call
                                               Opportunity__r.Cart_State__c, Opportunity__r.Cart_ZipCode__c,
                                               // DEF-0780 - retrieve from cached info
                                               eComm_Small_Image__c, Item_Color__c 
                                               from Shopping_cart_line_item__c where id=:lineItemId];
        System.debug('lineitem----'+lineItem);
        
        //REQ-438
        Decimal itemTotalPrice = 0.0;
        if(lineItem.List_Price__c != null && lineItem.Quantity__c != null){
            itemTotalPrice = lineItem.List_Price__c*lineItem.Quantity__c;
        }
        
        if(lineItem.DiscountType__c=='Percent'){
            lineItem.Flat_Discounted_Amount_On_Total__c =itemTotalPrice - lineItem.Last_Price__c;
        }else if(lineItem.DiscountType__c=='Flat'){
            lineItem.Flat_Discounted_Amount_On_Total__c =lineItem.Flat_Discounted_Amount__c * lineItem.Quantity__c;
            Decimal discountDoublePlus = 10000*(itemTotalPrice - lineItem.Last_Price__c)/itemTotalPrice;
            lineItem.Discount__c = discountDoublePlus.round(System.RoundingMode.HALF_UP)/100;
        }else{
            lineItem.Flat_Discounted_Amount_On_Total__c =0;
            lineItem.Discount__c =0;
        }
        lineItem.List_Price_on_Total__c = lineItem.Last_Price__c;
        
        return lineItem;
    }
    
    @AuraEnabled
    public static Boolean isLineItemLocked(String lineItemId) {  
        return Approval.isLocked(lineItemId);
    }

    
    @AuraEnabled
    public static String updateAsIs(String lineItemId, Boolean asIs) {  
        Shopping_cart_line_item__c lineItem = getLineItemDetail(lineItemId);
        lineItem.As_Is__c = asIs; 
        update lineItem;
        return 'Success';
    }
    
    @AuraEnabled
    public static String updateQuantity(String lineItemId, Decimal num) {  
        Shopping_cart_line_item__c lineItem = getLineItemDetail(lineItemId);
        lineItem.Quantity__c = num; 
        lineItem.Flat_Discounted_Amount_On_Total__c = lineItem.Flat_Discounted_Amount__c * num;
        if(lineItem.DeliveryDate__c != null)
        {
            lineItem.DeliveryDate__c=null;
        }
        update lineItem;
        return 'Success';
    }
    @AuraEnabled
    public static List<lineItemWrapper> getATPResponse(String sourceURL,List<lineItemWrapper> lineItem,Integration_Settings__c pmtTypesConf) {
        String endPointURL;
        for(lineItemWrapper wrapper:lineItem)
        {  
            if(wrapper.item.Delivery_Mode__c == 'HD'){
                String[] urlArr = sourceURL.split('&pc');
                if(!urlArr.isEmpty()){
                    endPointURL=urlArr[0];
                }
                
            }
            else if(wrapper.item.Delivery_Mode__c == 'DS'){
                endPointURL=sourceURL;
            }
            
            
        }
        //String endPointURL='https://ashley-preprod-dev.apigee.net/inventory/retail?as=8888300-164&sqt=D553-224|3|false&pc=30004';                   
        system.debug('endPointURL-->'+endPointURL);
        ShoppingCartItemDetailsAPIHelper itemDetailAPIHelper = new ShoppingCartItemDetailsAPIHelper();
        String resJSON = itemDetailAPIHelper.connectToAPIGetJSON(endPointURL,new Map<String, String> {'apiKey'=>pmtTypesConf.API_Key__c});
        JSONParser parser = JSON.createParser(resJSON);
        ATPWrapper ATPResponse=(ATPWrapper)parser.readValueAs(ATPWrapper.class);
        system.debug('ATPResponse-->'+ATPResponse);
        for(lineItemWrapper wrapper:lineItem)
        {
            if(wrapper.item.Delivery_Mode__c == 'HD' || wrapper.item.Delivery_Mode__c == 'DS'){
                if(!ATPResponse.entities.isEmpty())
                {
                    for(EntitiesWrapper entity:ATPResponse.entities){
                        
                        if(entity.sku == wrapper.item.Product_SKU__c)
                        {
                            wrapper.msg=entity.message;
                            
                        }
                        
                    }
                }
                
                else{
                    wrapper.msg='This item is not presently available in store. Please select another delivery method.';
                }
            }
        }
        system.debug('lineItem after ATP-->'+lineItem);
        return lineItem;
    }
    // DEF-0780 - ATC call
    @AuraEnabled
    public static DateTime getBestDate(Shopping_cart_line_item__c detail) {
        try {
            if(detail.Delivery_Mode__c == 'TW') {
                return system.today();
            }
            else {
                // store info and delivery window will be retrieved from API if needed and set already by ShoppingCartDetailCmpCtrl
                // so this method will just query
                StoreInfoWrapper si = ShoppingCartDetailCmpCtrl.getStoreInfoInShoppingCartDetail();
                Delivery_Window__c deliveryWindowSetting = Delivery_Window__c.getInstance('Delivery Window');
                Integer window = deliveryWindowSetting != null ? (Integer) deliveryWindowSetting.Window__c : null;
                DateTime bestDate;
                if(window != null) {
                    Date preferredDeliveryDateCoverted = date.today();
                    preferredDeliveryDateCoverted=preferredDeliveryDateCoverted.addDays(1);
                    API_ATCClient.ATCPayload  payload = new API_ATCClient.ATCPayload();
                    payload.deliveryMode=detail.Delivery_Mode__c;
                    
                    payload.region = detail.Opportunity__r.Cart_State__c;
                    payload.postalCode = detail.Opportunity__r.Cart_ZipCode__c;
                    
                    payload.products.add(new API_ATCClient.ProductWrapper(detail.Product_SKU__c, Integer.valueOf(detail.Quantity__c)));
                    
                    API_ATCClient.ATCResponseWrapper apiCallResponse = API_ATCClient.getAvailableDeliveryWindows(si.acctNo, si.RDC,preferredDeliveryDateCoverted, preferredDeliveryDateCoverted.addDays(window), payload);
                    if(!apiCallResponse.deliveryWindows.isEmpty())
                    {
                        Date windowDt=apiCallResponse.deliveryWindows[0].getStartDate();
                        Datetime dt = datetime.newInstance(windowDt.year(), windowDt.month(),windowDt.day());
                        bestDate=dt;
                    }
                }
                else {
                    throw new AuraHandledException('Error in retrieving Delivery Window');
                }
                return bestDate;
            }
        }
        catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}