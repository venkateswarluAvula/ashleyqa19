/*
Wrapper class to parse technician schedule work load API response
*/
public class TechWorkLoadWrap {
    @AuraEnabled
    public integer TechId { get; set; }
    @AuraEnabled
    public string FirstName { get; set; }
    @AuraEnabled
    public string LastName { get; set; }
    @AuraEnabled
    public string VendorId { get; set; }
    @AuraEnabled
    public string CompanyName { get; set; }
    @AuraEnabled
    public string ZipCode { get; set; }
    @AuraEnabled
    public integer MaxHoursPerDay { get; set; }
    @AuraEnabled
    public integer MaxRepairPerDay { get; set; }
    @AuraEnabled
    public integer MaxPiecesPerDay { get; set; }
    @AuraEnabled
    public integer CasegoodsSkillLevel { get; set; }
    @AuraEnabled
    public integer UpholsterySkillLevel { get; set; }
    @AuraEnabled
    public integer LeatherSkillLevel { get; set; }
    @AuraEnabled
    public string WeeklySchedule { get; set; }
    @AuraEnabled
    public object StreetAddress { get; set; }
    @AuraEnabled
    public object City { get; set; }
    @AuraEnabled
    public string StateCode { get; set; }
    @AuraEnabled
    public string HomePhone { get; set; }
    @AuraEnabled
    public string CellPhone { get; set; }
    @AuraEnabled
    public string EmailID { get; set; }
    @AuraEnabled
    public string FaxNumber { get; set; }
    @AuraEnabled
    public list<LeaveDateWrap> LeaveDates { get; set; }

    public class LeaveDateWrap {
        @AuraEnabled
        public string LeaveDate { get; set; }
        @AuraEnabled
        public string CreatedTime { get; set; }
        @AuraEnabled
        public integer CreatedUserID { get; set; }
    }
}