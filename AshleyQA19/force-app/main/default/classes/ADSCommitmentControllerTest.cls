@iSTest
public class ADSCommitmentControllerTest {
    
    static testMethod void insrtCommitment(){
        string InsertedRecId = 'a0q4F000000bjphQAA';
        
        Account acc = new Account(name = 'Test account');
        insert acc;
        
        opportunity opp = new opportunity();
        opp.name='test opp';
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ADS Detail Page').getRecordTypeId();
        opp.Opportunity_Type__c = 'ADS';
        opp.Type = 'New Business';
        opp.StageName = 'Open';
        opp.CloseDate = date.today() + 60;
        insert opp;
        
        List<ADS_Zip__c> lstADSZip = new List<ADS_Zip__c>();
        ADS_Zip__c adszip = new ADS_Zip__c();
        adszip.Market__c = 'Winchester Mkt';
        lstADSZip.add(adszip);  
        insert lstADSZip;
        
        Segment__c objSegment = new Segment__c();
        objSegment.Origin__c =  'Advance Mkt';
        objSegment.Destination__c = 'Billings Mkt'; 
        objSegment.Stop__c = 'testStop';
        insert objSegment;

        // List<ADS_Lane__c> myadslist = new List<ADS_Lane__c>();
        ADS_Lane__c adsl = new ADS_Lane__c(of_Trips_Per_Week__c = 10,ADS_Lane_Origin_Market__c = 'Advance Mkt',ADS_Lane_Destination_Market__c = 'Billings Mkt',
                                           ADS_Lane_Origin_State__c = 'NC',Opportunity__c = opp.Id,ADS_Lane_Origin_City__c = 'A M F GREENSBORO',
                                           ADS_Lane_Destination_City__c = 'ABSAROKEE',ADS_Lane_Destination_State__c = 'MT');
        
        insert adsl;
           
        
        Commitment__c com = new Commitment__c();
        com.Origin_Market__c = 'Advance Mkt';
        com.Destination_Market__c = 'Billings Mkt';
        com.Number_Trips_Required__c = 10;
        com.Status__c = 'Working';
        insert com;
        
        List<Commitment_Opportunities__c> ComOpplist = new List<Commitment_Opportunities__c>();
        Commitment_Opportunities__c ComOpp = new Commitment_Opportunities__c();
        ComOpp.Lanes__c = 2;
        ComOpp.Trips_Per_Week__c =com.Number_Trips_Required__c;
        ComOpp.Commitment__c =com.Id;
        ComOpp.Opportunity__c =opp.Id;
        ComOpplist.add(ComOpp);
        insert ComOpplist;
        
        Commitment__c coms = new Commitment__c();
        coms.Origin_Market__c = 'Advance Mkt';
        coms.Destination_Market__c = 'Billings Mkt';
        coms.Number_Trips_Required__c = 10;
        coms.Status__c = 'Working';
        
        Test.startTest();
        ADSCommitmentController.getMarketValues();
        ADSCommitmentController.updtRecord(com);
        ADSCommitmentController.CalculateValues(com.Origin_Market__c, com.Destination_Market__c, com.Id);
        ADSCommitmentController.createRecord(coms);
        ADSCommitmentController.laneresult adscc = new ADSCommitmentController.laneresult();
        adscc.OppId = opp.id;
        adscc.oppname = opp.name;
        adscc.laneNames = 'test name';
        adscc.AccountName = 'tets name';
        adscc.oppstage = 'tets name';
        adscc.trps = 2;
        adscc.cnt = 2;
        Test.stopTest();
        
    }
}