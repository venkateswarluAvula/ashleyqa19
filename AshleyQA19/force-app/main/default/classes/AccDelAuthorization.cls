public class AccDelAuthorization {

    
    public class oAuthJson{
        public String token_type;   //Bearer
        public String scope;    //user_impersonation
        public String expires_in;   //3600
        public String ext_expires_in;   //3600
        public String expires_on;   //1548744348
        public String not_before;   //1548744348
        public String resource; //https://5a9d9cfd-c32e-4ac1-a9ed-fe83df4f9e4d/cara-api-qa
        public String access_token; //eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Im5iQ3dXMTF3M1hrQi14VWFYd0tSU0xqTUhHUSIsImtpZCI6Im5iQ3dXMTF3M1hrQi14VWFYd0tSU0xqTUhHUSJ9.eyJhdWQiOiJodHRwczovLzVhOWQ5Y2ZkLWMzMmUtNGFjMS1hOWVkLWZlODNkZjRmOWU0ZC9jYXJhLWFwaS1xYSIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzVhOWQ5Y2ZkLWMzMmUtNGFjMS1hOWVkLWZlODNkZjRmOWU0ZC8iLCJpYXQiOjE1NDg3NDA0NDgsIm5iZiI6MTU0ODc0MDQ0OCwiZXhwIjoxNTQ4NzQ0MzQ4LCJhY3IiOiIxIiwiYWlvIjoiQVNRQTIvOEtBQUFBbUpjUzg4RXhCTGNRcG5xUHo2S2xMMXpkMWVVMTROeW92M084MXZPOEdWWT0iLCJhbXIiOlsicHdkIl0sImFwcGlkIjoiYWE0OGZlMTEtNGU0Mi00ZDBlLWJkNDMtNWE2YjZkYmE4ZGU5IiwiYXBwaWRhY3IiOiIxIiwiZmFtaWx5X25hbWUiOiJzX0NSTVFBQ2FyYSIsImlwYWRkciI6IjI3LjUuNzIuMzQiLCJuYW1lIjoic19DUk1RQUNhcmEiLCJvaWQiOiJlZDk0MGM3Zi0wMjEwLTQ0ZGUtYjI5OC01YWIwODI2ZGM3MTciLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtMzM5MjIwNjA3OC03MTg0MDUxMzEtMzAxMzI4MTI0NC0yNDMzMzAiLCJzY3AiOiJ1c2VyX2ltcGVyc29uYXRpb24iLCJzdWIiOiJGYmFnNWJWTUg2clhTWnJsVjd6NzNEQl90UDNPbm5pLW5oY3hjM1ptdjNnIiwidGlkIjoiNWE5ZDljZmQtYzMyZS00YWMxLWE5ZWQtZmU4M2RmNGY5ZTRkIiwidW5pcXVlX25hbWUiOiJzX0NSTVFBQ2FyYUBhc2hsZXlmdXJuaXR1cmUuY29tIiwidXBuIjoic19DUk1RQUNhcmFAYXNobGV5ZnVybml0dXJlLmNvbSIsInV0aSI6IjUtYXpYNDZvU0VLZmJaMHBMVlVPQUEiLCJ2ZXIiOiIxLjAifQ.iNC2TVsT5WtoUcR3oD4djnJgkXgGNQ268c0TSeL7IrBcrMhqNVVWYasI4XdlW8nwf8KS_Dt3Hvtke7k5I6fXuMmFtotVEzTpHh4lg5yLzAmYXJEFw4RCavyhcAkmFXXZGJeAFY8oDe6nz10uPyWMr_UQW6eVsMi0Yhg1n6agEQ6a_oAd_3pOw5-PbQXbWQmWpBboWphW_s_bCFca7DNbk2yLDt6CPDb-5tOJwh814URPiow1b1EZQk3ELO5jqiylc-bJ-7nhdH74a6vOadtk6QXBGZ94o47zXQIW6hsVvDkIfx_Ibu9to2BvSS66T6Hsb-He7QDvmBUPMDsnJ5kHjw
        public String refresh_token;    //AQABAAAAAACEfexXxjamQb3OeGQ4GugvxP5FXX_87uQnX4vASRJX-J54L2dQep0uZXHc6t1soHPnS4CQJdGsRUTfxI1ZVNy5cocaW33KB5UWykJy9KXO1KJYjs7wtPPwpZJckkirgTAUd4FrPd1a-vFkjQxXKuXUF7x1GMAja5YJa93YeZVNOddQqXqK8faWS7kkGGhTJaJnJTQuu4_Zag9O-cZjlSP9joa2AJoCpF9cixZ3KmI4F5rQ93OkZKtFVRXLUu_Gv8MKZa5s8NS3VL28dn4nseWtfGv2HKltxbT3zEdiKaewqmmHdEAuhFCZwQ7PRLBDSkFH_yjzyyI9Rzk_EN2Ojlm2-cgErMPCTe4lrRqWsQswfcqL-RreaBhw6-6ajOMsO60QVrHbhuq4Lbv7MfLTWCyR0itxOzvjNWkv2jAIT5OkhNkAa4zn9FFv24375ZnVhcxjho8wRwWJQ1Rep3adXKg3zQ-FXi25emW6TQBSPYXiVhcAy-llidNXEMPAXreJ2tqnO5UEmOSc0x0IVsrNygAS7CVk28_B7UjMU0odFqLLFMIZGX4bI4Zs0zoovk7nDl2P2Chjj466MBtz8lqm_uyLeMnP9HWcB4N4A--b5Qr-_4TPx15OxnypFlKgY0OUC8QWUC4IoK-hgE5DepIq2j1Vj2IHnhanJFagLA7OrYPQD_Fn7HIFuPixq_BcezXcZ3C4NPLi-VGVkr-9kYOuMQIwhMntLYdS7lFMiQtZ1onDyCw3UodL454zBKoL9-ONAvT17zuRtDGAr8GsmTrD0AdtIAA
        public String id_token; //eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiJhYTQ4ZmUxMS00ZTQyLTRkMGUtYmQ0My01YTZiNmRiYThkZTkiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC81YTlkOWNmZC1jMzJlLTRhYzEtYTllZC1mZTgzZGY0ZjllNGQvIiwiaWF0IjoxNTQ4NzQwNDQ4LCJuYmYiOjE1NDg3NDA0NDgsImV4cCI6MTU0ODc0NDM0OCwiYW1yIjpbInB3ZCJdLCJmYW1pbHlfbmFtZSI6InNfQ1JNUUFDYXJhIiwiaXBhZGRyIjoiMjcuNS43Mi4zNCIsIm5hbWUiOiJzX0NSTVFBQ2FyYSIsIm9pZCI6ImVkOTQwYzdmLTAyMTAtNDRkZS1iMjk4LTVhYjA4MjZkYzcxNyIsIm9ucHJlbV9zaWQiOiJTLTEtNS0yMS0zMzkyMjA2MDc4LTcxODQwNTEzMS0zMDEzMjgxMjQ0LTI0MzMzMCIsInN1YiI6ImFVRGRYME5NZHNUUmlKREYtNjU5Y19mSWk5ZktSLVNUeWl3MWxidVZFM1UiLCJ0aWQiOiI1YTlkOWNmZC1jMzJlLTRhYzEtYTllZC1mZTgzZGY0ZjllNGQiLCJ1bmlxdWVfbmFtZSI6InNfQ1JNUUFDYXJhQGFzaGxleWZ1cm5pdHVyZS5jb20iLCJ1cG4iOiJzX0NSTVFBQ2FyYUBhc2hsZXlmdXJuaXR1cmUuY29tIiwidmVyIjoiMS4wIn0.
    }
    
    public String accessTokenData()
    {
           String body = 'grant_type=password';
              
            body += '&client_secret=' + EncodingUtil.urlEncode(Label.Ashley_RoutingClientSecret, 'UTF-8');
            body += '&client_id=' + EncodingUtil.urlEncode(Label.Ashley_RoutingClientId, 'UTF-8');
            body += '&scope=' + EncodingUtil.urlEncode('openid', 'UTF-8');
            body += '&resource=' + EncodingUtil.urlEncode(Label.Ashley_Routingresource, 'UTF-8');
            body += '&username=' + EncodingUtil.urlEncode(Label.Ashley_RoutingUserName, 'UTF-8');
            body += '&password=' + EncodingUtil.urlEncode(Label.Ashley_RoutingPassword, 'UTF-8');
           
            String ePnt =  Label.Ashley_TokenGenerationURL;
                    
        	Http http = new Http();
            HttpRequest httpReq = new HttpRequest();
            HttpResponse httpResp = new HttpResponse();
            httpReq.setEndpoint(ePnt);
            httpReq.setBody(body);
            httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            httpReq.setMethod('POST');  
            httpResp = http.send(httpReq); 
            system.debug('resp-------'+httpResp.getBody());
            
           oAuthJson oaJson = (oAuthJson)JSON.deserialize(httpResp.getBody(), oAuthJson.class); 
           system.debug('resp_access_token-------'+oaJson.access_token); 
           return oaJson.access_token;
    }
}