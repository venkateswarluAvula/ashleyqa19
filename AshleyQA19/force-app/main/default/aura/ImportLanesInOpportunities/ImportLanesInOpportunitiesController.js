({
    openModal : function(component, event, helper) { 
        console.log("openModal---"+component.get("v.openModal"));
        if(component.get("v.openModal") == true){
            component.set("v.openModal",false);
        }
        else{
        	component.set("v.openModal",true);
        }
    },
    exportCSV: function(component, event, helper) {
        console.log("openModal---"+component.get("v.openModal"));
        if(component.get("v.exportModal") == true){
            component.set("v.exportModal",false);
        }
        else{
            component.set("v.exportModal",true);
            console.log('Lane onload');
            var oppId = component.get("v.recordId");
            console.log('@@@ oppId' + oppId);
            var action = component.get("c.fetchLane");
            action.setParams({
                "oppId" : oppId
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('Lane response' + response.getReturnValue());
                    component.set('v.ListOfLane', response.getReturnValue());
                }
                console.log('Lane to export' + component.get("v.ListOfLane"));
            });
            $A.enqueueAction(action);
        }
    },
    closeModel: function(component, event, helper) {
      component.set("v.openModal", false);
    },
    ImportData: function (component, event, helper) {
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        var oppId = component.get("v.recordId");
        if (file) {
            console.log("File");
            var reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            reader.onload = function (evt) {
                console.log("EVT FN");
                var csv = evt.target.result;
                console.log('CSV data:'+ csv);
                var result = helper.CSV2JSON(component,csv);
                console.log('csv result = ' + result);
                helper.CreateLane(component,result);
                component.set("v.isLoading",false);
                component.set("v.openModal",false);
            }
            reader.onerror = function (evt) {
                console.log("error reading file");
            }
        }
	},
    ExportData: function (component, event, helper) {
      //  helper.onLoad(component);
        component.set("v.isLoading",true);
        var stockData = component.get("v.ListOfLane");
        console.log('Lane stock' + stockData);
        var csv = helper.convertArrayOfObjectsToCSV(component,stockData);   
        if (csv == null){return;} 
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'ExportData.csv'; 
        document.body.appendChild(hiddenElement); 
        component.set("v.isLoading",false);
        hiddenElement.click();
        component.set("v.exportModal",false);
    }
})