({
    sendHelper: function(component,event,helper, getEmail, getSubject, getbody,getrecid) {
        console.log('helperrecid'+getrecid);
        // call the server side controller method 	
        var action = component.get("c.sendMailMethod");
        var templateId = component.get("v.selTempl");
        var frommail = component.find("From").get("v.value");
        console.log('fromtooo-->'+frommail);
        console.log('bodtyyy--'+getbody);
        // set the params to sendMailMethod method 
        action.setParams({
            'mMail': getEmail,
            'mSubject': getSubject,
            'mbody': getbody,
            'recid':getrecid,
            'fmail':frommail,
            'templateId':templateId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.mailStatus", true);
            }
            
        });
        $A.enqueueAction(action);
    },
    getEmailTempaltes : function(component, event) {
        var action = component.get("c.getTemplates");
        
        action.setCallback(this,function(response){
            var loadResponse = response.getReturnValue();
            console.log('templates..!',loadResponse);
            
            if(!$A.util.isEmpty(loadResponse)){
                component.set('v.templates',loadResponse);
                
            }
        });
        $A.enqueueAction(action);
    },
    
    getTemplate : function(component, event) {
        var templId = component.get("v.selTempl");
        component.set("v.showLoader", true);
        if(!$A.util.isEmpty(templId)){
            var action = component.get("c.getTemplateDetails");
            action.setParams({"templteId":templId});
            
            action.setCallback(this,function(response){
                component.set("v.showLoader", false);
                var responseVal = response.getReturnValue();
                console.log('responseVal..@getTemplate ',responseVal);
                
                if(!$A.util.isEmpty(responseVal)){
                    component.set("v.templDetail",responseVal);
                    component.set("v.subjTxt",responseVal.Subject);
                    if(!$A.util.hasClass(component.find("body"), "slds-hide")){
                        $A.util.addClass(component.find("body"), 'slds-hide'); 
                    }
                    
                }
            });
            $A.enqueueAction(action);
        }
        else {
            component.set("v.showLoader", false);
            if($A.util.hasClass(component.find("body"), "slds-hide")){
                $A.util.removeClass(component.find("body"), 'slds-hide');
            }
        }
    }
})