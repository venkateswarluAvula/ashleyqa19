({
    openTab : function(component, event, helper) {
        
        var workspaceAPI = component.find("workspace");
        var recId = component.get("v.recordId");
        
        workspaceAPI.isConsoleNavigation().then(function(response) {
            console.log('Console Navigation Resp: ' + response);
            if(response) {    
                workspaceAPI.openTab({
                    url: "/lightning/r/SalesOrder__x/"+recId+"/view",
                    focus: true
                }).then(function(response) {
                    console.log('response--'+response);
                    workspaceAPI.openSubtab({
                        parentTabId: response,
                        pageReference: {
                            "type": "standard__component",
                            "attributes": {
                                "componentName": "c__EditSalesorderRecord",
                                "recordId":"recId"
                            }
                        },
                    });
                    focus: true
                }).catch(function(error) {
                    console.log(error);
                });
            } else {
                console.log('Console view is not supported');
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:EditSalesOrderRecord",
                    componentAttributes: {
                        recordId : recId
                    }
                });
                evt.fire();
            }
        })
        .catch(function(error) {
            console.log('Console Navigation Err: ' + error);
        });
    }
    
})