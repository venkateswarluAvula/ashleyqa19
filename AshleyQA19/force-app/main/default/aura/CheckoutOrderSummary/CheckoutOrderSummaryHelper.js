({
    showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
        });
        toastEvent.fire();
    },
    getCustomerInfo: function(component,helper) {
        
        var action = component.get("c.getShoppingCart");
        console.log(component.get('v.recordId'));
        action.setParams({"accountId" : component.get('v.recordId')});
        
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setCallback(this, function(response){
            // to avoid multiple spinners issue
            //  this.pushToListOfCompletedEvents(component, "getCustomerInfo");
            toastErrorHandler.handleResponse(
                response,
                function(response){
                    var rtnValue = response.getReturnValue();
                    if (!$A.util.isEmpty(rtnValue) && !$A.util.isEmpty(rtnValue.Id)) {
                        
                        component.set("v.opp", rtnValue);
                        var opp = component.get("v.opp");
                        //   component.set("v.shoppingCartDiscount",Math.round(100*opp["Shipping_Discount__c"])/100);
                        component.set("v.DiscountStatusUpdate",false);
                        
                    }else {
                        helper.showToast("error", 'Could not find this cart', component,
                                         'Couldn not find this cart, please confirm this cart is valid!');                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Could not find this cart', component,
                                     message); 
                }
            )
        });            
        action.setBackground();
        $A.enqueueAction(action); 
    },
    SaveDiscount: function(component,helper){
        var approverId =  component.get("v.opp")["Discount_Approver__c"];
        var discountReasonCode = component.find("discountReasonOption").get("v.value"); 
        var percentDiscountAmount = Number(component.find("percentDiscountAmount").get("v.value"));
        var flatDiscountAmount = parseInt(component.find("flatDiscountAmount").get("v.value"));
        var itemsPrice = parseInt(component.find("itemsPrice").get("v.value"));
       // alert('percentDiscountAmount '+percentDiscountAmount);
        var FlatdiscountAmount = component.get("v.shoppingCartFlatDiscount");
        
        //alert('percentDiscountAmount'+percentDiscountAmount+discountReasonCode+approverId);
        // alert('FlatdiscountAmount'+FlatdiscountAmount);
        // alert('approverId'+approverId);
        var hasError = false;
        if(percentDiscountAmount<0 ){
            helper.showToast("error", 'Please Input Valid Discount Amount.', component,
                             'You are trying to apply a negative discount, Please input valid discount amount to continue.');   
            
            hasError = true;  
        }
        if(isNaN(percentDiscountAmount)){
            helper.showToast("error", 'Please Input Valid Discount Amount.', component,
                             'You are trying to apply a without discount, Please input valid discount amount to continue.');   
            hasError = true;
        }
        if(isNaN(flatDiscountAmount)){
            helper.showToast("error", 'Please Input Valid Flat Discount Amount.', component,
                             'You are trying to apply a without Valid Flat Discount Amount, Please input valid Valid Flat Discount Amount to continue.');   
            hasError = true;
        }
        if(isNaN(itemsPrice)){
            helper.showToast("error", 'Please Input Valid Price Amount.', component,
                             'You are trying to apply a without Price , Please input valid Price amount to continue.');   
            hasError = true;
        }
        
        if(percentDiscountAmount>100){
            helper.showToast("error", 'Please Input Valid Discount Amount.', component,
                             'You are trying to apply a large discount exceed the product original price, Please input valid discount amount to continue.');   
            
            hasError = true;
        }
        if(discountReasonCode=="null"){
            helper.showToast("error", 'Please Select A Discount Reason.', component,
                             'You are trying to apply a discount, Please select discount reason to continue.');   
            hasError = true;
        }
        if(approverId=='' || approverId==undefined ){
            helper.showToast("error", 'Please Select An Approver.', component,
                             'The discount you are trying to apply requires approval. Please select an approver to continue.');   
            hasError = true;
        }
        if(hasError){
            return;
        }
        
        var action = component.get("c.saveShippingDiscount");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setParams({"accountId" : component.get('v.recordId'),
                          "percentDiscountAmount" : percentDiscountAmount,
                          "discountReasonCode" : discountReasonCode=="null"?"":discountReasonCode,//Fix Post mentioned error of DEF-0772 on 8/8/2018  
                          "approverId" : approverId,
                          "FlatDiscountAmount" : FlatdiscountAmount
                         });
        
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                response, // handle failure
                function(response){ 
                    var rtnValue = response.getReturnValue();
                    // alert('rtnValue'+rtnValue);
                    if (rtnValue !== null && rtnValue=='Success') { 
                        
                        helper.getCheckoutSummary(component, helper);
                        helper.getCustomerInfo(component,helper);
                        var eventComponent = component.getEvent("DiscountChangeNotifyToCheckoutSummary");
                        eventComponent.fire();
                        
                        component.set("v.showDiscountModal",false);
                        component.set("v.DeliveryIcon",false);
                        
                    } 
                    else {
                        helper.showToast("error", 'Failed to save Discount', component,
                                         'Failed to save Discount.'); 
                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Failed to save Discount', component,
                                     message);
                    
                }
            )            
        });        
        action.setBackground();
        $A.enqueueAction(action); 
    },
    
    closeModal : function(component,containerName){
        var popUp = component.find(containerName);
        console.log(popUp);
        $A.util.removeClass(popUp, 'slds-fade-in-open');
        $A.util.addClass(popUp, 'slds-fade-in-close');
        var backdrop = component.find("backdropContainer");
        console.log(backdrop);
        $A.util.removeClass(backdrop, 'slds-modal-backdrop--open');
        $A.util.addClass(backdrop, 'slds-modal-backdrop--close');
    },
    getDiscountReasonList: function(component,helper) {
        var action = component.get("c.getOPPDiscountReasonList");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setCallback(this, function(response){
            // to avoid multiple spinners issue
            //  this.pushToListOfCompletedEvents(component, "getDiscountReasonList");
            toastErrorHandler.handleResponse(
                response,
                function(response){
                    var rtnValue = response.getReturnValue();
                    console.log(rtnValue);
                    if (rtnValue !== null) {    
                        console.log(rtnValue);
                        component.set("v.discountReasonList", rtnValue);
                    }else {
                        helper.showToast("error", 'Discount Reason are empty', component,
                                         'Could not find Discount Reason  .');                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Discount Reason  are empty', component,
                                     message);
                }
            )
        });            
        action.setBackground();
        $A.enqueueAction(action); 
    },
    getAccountStoreInfo: function(component,helper) {
        var action = component.get("c.getRSAOneSourceId");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setCallback(this, function(response){
            // to avoid multiple spinners issue
            //this.pushToListOfCompletedEvents(component, "getAccountStoreInfo");
            toastErrorHandler.handleResponse(
                response,
                function(response){
                    var rtnValue = response.getReturnValue();
                    console.log(rtnValue);
                    if (rtnValue !== null) {    
                        console.log(rtnValue);
                        component.set("v.accountStoreInfo", rtnValue);
                    }else {
                        helper.showToast("error", 'RSA One Source Id Info are empty', component,
                                         'Could not find RSA One Source Id Info.');                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'RSA One Source Id Info are empty', component,
                                     message);
                }
            )
        });            
        action.setBackground();
        $A.enqueueAction(action); 
    },
    getCheckoutSummary : function(component,helper) {
        // alert('entered');
        
        var shipAddr = component.get("v.shipToAddress");
        var action = component.get("c.getCheckoutSummaryInfo");
        console.log(component.get('v.recordId'));
        action.setParams({"accountId" : component.get('v.recordId'),
                          "shipAddr" :shipAddr
                         });
        
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                response,
                function(response){
                    var rtnValue = response.getReturnValue();
                    console.log(rtnValue);
                    if (rtnValue !== null) {    
                        component.set("v.shoppingCartDeliveryFee", rtnValue["TotalDeliveryAmount"]);
                        component.set("v.shoppingCartTotal", rtnValue["SalesGrandTotal"]);
                        component.set("v.shoppingCartTotalBeforeTax", rtnValue["TotalProductLastAmount"]);  
                        component.set("v.shoppingCartEstimatedTax", rtnValue["TotalTaxAmount"]);
                        component.set("v.shoppingCartWarrantyFee", rtnValue["TotalWarrantyAmount"]);  
                        component.set("v.shoppingCartRecycleFee", rtnValue["TotalRecycleAmount"]);
                        component.set("v.shoppingCartNextDayCharge", rtnValue["TotalNextDayCharge"]);
                        component.set("v.shoppingCartOriginalPrice", rtnValue["OriginalPrice"]);
                        component.set("v.tempvalextendedprice", rtnValue["ExtendedPrice"]);
                       // alert('original price'+rtnValue["ExtendedPrice"]);
                        component.set("v.shoppingCartWarrantyItemFee", rtnValue["WarrantyItemAmount"]);
                        component.set("v.isRenderForUpdate",false);
                        
                    }else {
                        helper.showToast("error", 'Tax Info is empty', component,
                                         'Could not get Tax Info.');                        
                    }
                },
                function(response, message){ // report failure
                    // DEF-0777 fix
                    helper.showToast("error", '', component,
                                     message);
                }
            )
        });            
        action.setBackground();
        $A.enqueueAction(action); 
    }
    
})