({
    
    doInit : function(component, event, helper) {
        var action = component.get('c.getcaseHistoryrecords');
        var currentRecId = component.get("v.recordId");
        //var currentRecId = '500q000000J7vjzAAB';
        action.setParams({
            parentRecordId : currentRecId
        });
        action.setCallback(this, function(actionResult) {
            var succesData = actionResult.getReturnValue();
            component.set('v.CaseHis', succesData);
            component.set("v.CaseCount",succesData.length);
            
        });
        $A.enqueueAction(action);
    },
    
    openRelatedList : function(component, event, helper) {
        //alert('recid1--'+component.get("v.recordId"));
        var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        relatedListEvent.setParams({
            "relatedListId": "Histories",
            "parentRecordId": component.get("v.recordId")
        });
        relatedListEvent.fire();
    },
    
    doInit1: function(component, _event){
        var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        var recid = component.get("v.recordId");
        //console.log('recid2--'+component.get("v.recordId"));
        relatedListEvent.setParams({
            "relatedListId": "Histories",
            "parentRecordId": recid
        });
        relatedListEvent.fire();
    }
})