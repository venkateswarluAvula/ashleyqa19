({
    showToast : function(type, title, component, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: type,
            title: title,
            message: message,
        });
        toastEvent.fire();
    },
    initialLineItem: function(component, helper) { 
        var action = component.get("c.getLineItemDetail");
        var toastErrorHandler = component.find('toastErrorHandler');
        //alert('line id'+component.get('v.lineItemId'));
        
            action.setParams({"lineItemId" : component.get('v.lineItemId')});
            action.setCallback(this, function(response){
                toastErrorHandler.handleResponse(
                    response, // handle failure
                    function(response){ 
                        var rtnValue = response.getReturnValue();
                        if (rtnValue !== null) {
                          
                            component.set("v.lineItem", rtnValue);
                            var averageCost = rtnValue["Average_Cost__c"];
                            
                            var originalPrice = rtnValue["List_Price__c"];
                            var marginCriteria = component.get("v.marketDiscountCriteria");
                            var averageDiscountCriteria = (1-(averageCost*100)/(originalPrice*(100-marginCriteria)))*100;
                            if(isNaN(averageDiscountCriteria) || averageCost==0){
                                averageDiscountCriteria=0;
                            }
                            component.set("v.averageDiscountCriteria", averageDiscountCriteria);
                            
                          /*  $A.createComponent("ui:outputNumber", 
                                               {"value": averageDiscountCriteria}, 
                                               function(newCmp) {
                                                   var body = component.get("v.body");
                                                   body = [];
                                                   // newCmp is a reference to another component
                                                   body.push(newCmp);
                                                   component.set("v.body", body);
                                               }); */
                            // DEF-0780 - ATC call
                            if(!component.get("v.inCheckoutPage")) {
                                helper.lazyLoadBestDate(component, rtnValue);
                            }
                        } else {
                            helper.showToast("error", 'cart line Item not found', component,
                                             'Could not find cart line Item.');                        
                        }
                    },
                    function(response, message){ // report failure
                        helper.showToast("error", 'cart line Item not found', component,
                                         message);
                    }
                )            
            });        
            action.setBackground();
            $A.enqueueAction(action); 
    },
    // DEF-0780 - ATC call
    lazyLoadBestDate : function(component, lineItem) {
        var action = component.get("c.getBestDate");
        action.setParams({"detail" : lineItem});
        action.setCallback(this, function(response){
            var state = response.getState();
            var bestDate;
            if(state === "SUCCESS") {
                bestDate = response.getReturnValue();
                component.set("v.BestDate", bestDate);
                if(bestDate == null) {
                    component.set("v.bestDateMsg", 'Call');
                }
            }
            else if (state === "INCOMPLETE") {
                component.set("v.bestDateMsg", 'Error in loading best date');
            }
            else if (state === "ERROR") {
                component.set("v.bestDateMsg", 'Error in loading best date');
            }
        });
        action.setBackground();
        $A.enqueueAction(action); 
    },

    checkLineItemLocked:function(component, helper) {   
        var action = component.get("c.isLineItemLocked");
        var toastErrorHandler = component.find('toastErrorHandler');
            action.setParams({"lineItemId" : component.get('v.lineItemId')});
            action.setCallback(this, function(response){
                toastErrorHandler.handleResponse(
                    response, // handle failure
                    function(response){ 
                        var rtnValue = response.getReturnValue();
                        if (rtnValue !== null) {   
                            component.set("v.locked", rtnValue);
                        } else {
                            helper.showToast("error", 'cart line Item Lock status not found', component,
                                             'Could not find cart line Item Lock status.');                        
                        }
                    },
                    function(response, message){ // report failure
                        helper.showToast("error", 'cart line Item Lock status not found', component,
                                         message);
                    }
                )            
            });        
            action.setBackground();
            $A.enqueueAction(action); 
    },
    initialProductDetail: function(component, helper) {
       // alert('1');
        var action = component.get("c.getProductDetail");
        var toastErrorHandler = component.find('toastErrorHandler');
        //REQ-438, If item.Product_Title__c is empty, get Product_Title__c from product feed API
        var productTitle = component.get("v.lineItem")["Product_Title__c"];
        var lineItemId;
        if($A.util.isEmpty(productTitle)){
            lineItemId =component.get("v.lineItemId");
        }  
        
        action.setParams({"productSKUId" : component.get('v.productSKUId'),
                          "lineItemId" : lineItemId});
        //End REQ-438, If item.Product_Title__c is empty, get Product_Title__c from product feed API
            action.setCallback(this, function(response){
                toastErrorHandler.handleResponse(
                    response, // handle failure
                    function(response){ 
                        var rtnValue = response.getReturnValue();
                        if (rtnValue !== null) {   
                            component.set("v.productDetail", rtnValue);
                            //REQ-438, get Product_Title__c from product feed API  
                            var updatedLineItem =  component.get("v.lineItem");
                            updatedLineItem["Product_Title__c"]=rtnValue["productTitle"];
                            component.set("v.lineItem", updatedLineItem);
                        } else {
                            helper.getProductInfoFromUnbxd(component,helper);                           
                        }
                    },
                    function(response, message){ // report failure
                        helper.showToast("error", 'Product in cart not found', component,
                                         message);
                    }
                )            
            });        
            action.setBackground();
            $A.enqueueAction(action); 
    },
    getProductInfoFromUnbxd : function(component,helper) {   
         var currentIndex =0;
        var action = component.get("c.getProductListSearch");
      
            action.setParams({"searchKey" : component.get('v.productSKUId'),
                              "currentIndex" : currentIndex,
                              "rows" : 1});
            var toastErrorHandler = component.find('toastErrorHandler');
            action.setCallback(this, function(response){
                toastErrorHandler.handleResponse(
                    response,
                    function(response){
                        var rtnValue = response.getReturnValue();
                        if (rtnValue !== null) {    
                            var obj = JSON.parse(rtnValue);
                            if(obj["response"]["numberOfProducts"]<=0){  
                                return;
                            }
                            for(var i=0; i<  obj["response"]["products"].length; i++){
                                
                                if(!$A.util.isEmpty(obj["response"]["products"][i]["id"])){                
                                    obj["response"]["products"][i]["sku"]=obj["response"]["products"][i]["id"];
                                }else if(!$A.util.isEmpty(obj["response"]["products"][i]["uniqueId"])){ 
                                    //3X SKU search response uniqueId
                                    obj["response"]["products"][i]["sku"]=obj["response"]["products"][i]["uniqueId"];
                                }
                                
                                obj["response"]["products"][i]["productTitle"]=obj["response"]["products"][i]["name"];
                            }
                            var productDetail = obj["response"]["products"][0];
                            productDetail["ecommSmallImage"] = productDetail["smallImageUrl"];
                           /* 
                            productDetail["swatches"] = [];
                            productDetail["swatches"]["imageSource"] =productDetail["swatchesimageUrl"] ? productDetail["swatchesimageUrl"] : [];
                            productDetail["swatches"]["color"]  =productDetail["swatchesname"] ? productDetail["swatchesname"] : [];
                            productDetail["swatches"]["sku"] =  productDetail["childSku"] ? productDetail["childSku"] : [];
                            */
                            productDetail["itemColor"] = productDetail["color"] ? productDetail["color"][0] : [];
                            /*
                            productDetail["detailedDescription"] = productDetail["description"] ? productDetail["description"] :[];
                            productDetail["productDescription"] = productDetail["productDetails"] ? productDetail["productDetails"] :[];
                            console.log(productDetail["variants"]);
                            productDetail["variants1"] = [];
                            if(productDetail["variants"]){
                                for(var i=0; i<  productDetail["variants"].length; i++){
                                    var variantOpt =  JSON.parse(productDetail["variants"][i]);
                                    productDetail["variants1"][i] = {};
                                    productDetail["variants1"][i]["sku"]= variantOpt["id"] ;
                                    productDetail["variants1"][i]["size"]= variantOpt["size2"] ;
                                    productDetail["variants1"][i]["imageSource"] = variantOpt["largeImageUrl"];
                                    productDetail["variants1"][i]["productVariantUrl"] = variantOpt["url"];
                                    
                                }
                                productDetail["variants"] = [];
                                productDetail["variants"] = productDetail["variants1"]; 
                                console.log(productDetail["variants"]);
                            }
                           */
                            
                            component.set("v.productDetail",  productDetail);
                            //REQ-438, get Product_Title__c from unbxd API  
                            var productTitle = component.get("v.lineItem")["Product_Title__c"];
                            if($A.util.isEmpty(productTitle)){                                
                                var updatedLineItem =  component.get("v.lineItem");
                                updatedLineItem["Product_Title__c"]=productDetail["productTitle"];
                                component.set("v.lineItem", updatedLineItem);
                                helper.saveProductTitle(component, helper);
                            }
                        }
                    },
                    function(response, message){ // report failure
                        helper.showToast("error", 'Product '+ component.get('v.productDetailId') +'not found', component,
                                         message);
                    }
                )
            });            
            action.setBackground();
            $A.enqueueAction(action); 
        
    },
    saveProductTitle:function(component, helper) { 
         var action = component.get("c.saveProductTitle");
        action.setParams({"productTitle":component.get("v.lineItem")["Product_Title__c"],
                          "lineItemId" : component.get("v.lineItem")["Id"]});
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                    response, // handle failure
                    function(response){ 
                        
                    },
                    function(response, message){ // report failure
                        helper.showToast("error", 'Save Product Title Failed', component,
                                         message);
                    }
                )            
        });
    },
    
    updateAsIs: function(component, helper, lineItemId, value) { 
        console.log('setting as is ' + lineItemId);
        var action = component.get("c.updateAsIs");
        var toastErrorHandler = component.find('toastErrorHandler');
        action.setParams({"lineItemId" : lineItemId, 
                          "asIs" : value});
          
        action.setCallback(this, function(response){
            toastErrorHandler.handleResponse(
                response, // handle failure
                function(response){ 
                    var rtnValue = response.getReturnValue();
                    if (rtnValue !== null && rtnValue=='Success') {
                        //$A.get("e.force:refreshView").fire();
                        console.log('noting success');
                        helper.fireShoppingCartLineItemEvent(component, component.get("v.CART_ACTION_MARK_ITEM_AS_IS"), component.get('v.lineItemId'));
                        // per client preference, no toast shown for success
                    } else {
                        helper.showToast("error", 'Failed to update As Is value', component,
                                         'Failed to update As Is value.');                        
                    }
                },
                function(response, message){ // report failure
                    helper.showToast("error", 'Error updating As Is Value', component,
                                     message);
                }
            )            
        });        
        action.setBackground();
        $A.enqueueAction(action); 
    },    
    updateQuantity:function(component,quantity){
        var action = component.get("c.updateQuantity");
        console.log(quantity);
        action.setParams({
            "lineItemId":component.get('v.lineItemId'),
            "num":quantity
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var val=response.getReturnValue();
                //$A.get('e.force:refreshView').fire();
                this.fireShoppingCartLineItemEvent(component, component.get("v.CART_ACTION_UPDATE_QUANTITY"), component.get('v.lineItemId'));
            }
            
        });
        $A.enqueueAction(action); 
    },
                
    fireShoppingCartLineItemEvent : function(component, action, lineItemId){
        var event = component.getEvent("shoppingCartLineItemEvent");
        event.setParams({"action" : action, "lineItemId" : lineItemId });
        event.fire();
    }
   
})