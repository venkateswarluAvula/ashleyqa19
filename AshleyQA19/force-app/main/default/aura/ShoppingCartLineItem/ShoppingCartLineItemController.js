({
    doInit: function(component, event, helper) {
  
        component.set("v.currentUserId",$A.get("$SObjectType.CurrentUser.Id"));
        component.set("v.RendershpForUpdate");
        var productSku = component.get('v.productSKUId');
        if(productSku.startsWith('*')){
            component.set('v.isStarItem', true);
            helper.initialLineItem(component, helper); 
        }else{
            component.set('v.isStarItem', false);
            // DEF-0780 - no need to get product detail
            //helper.initialProductDetail(component, helper);
            helper.initialLineItem(component, helper);
            helper.checkLineItemLocked(component, helper);  
        }
        
    },
    changeAsIs: function(component, event, helper){
        var lineItemId = component.get("v.lineItemId");
        var aiv=component.get("v.lineItem.As_Is__c");
        helper.updateAsIs(component, helper, lineItemId, aiv);
    },
    moveToWishlist: function(component, event, helper){
        
    },
    navigateToProd: function(component, event, helper) {
        var selectedItem = event.currentTarget; 
        var productId = selectedItem.dataset.record;
        var appEvent = $A.get("e.c:NavigateToWrapperComponentAppEvent");
        appEvent.setParams({
            "targetCmpName" : 'ProductCategoryWrapperCmp',
            "targetCmpParameters" : {"parentCategoryId": null, "productDetailId": productId, "searchKey": null}
        });             
        appEvent.fire();  
    },
    mannualUpdateQuantity : function(component, event, helper) {
        var quantity =  component.find("itemQuantity").get("v.value");
        helper.updateQuantity(component,quantity);
    },
    openDiscountModal:function(component,event,helper){
        //event to notify Parent to openDiscountModal
        var eventComponent = component.getEvent("NotifyParentOpenDiscountModal");
        eventComponent.setParams({ "notifyParam":  component.get('v.lineItem')});
        eventComponent.setParams({ "notifyParam1": component.get('v.productDetail') });
        eventComponent.fire(); 
    },
      openDeleteModal:function(component,event,helper){
        //event to notify Parent to openDiscountModal
        var eventComponent = component.getEvent("NotifyParentOpenDeleteModal");
        eventComponent.setParams({ "notifyParam":  component.get('v.lineItem')});
        eventComponent.setParams({ "notifyParam1": component.get('v.productDetail') });
        eventComponent.fire(); 
    },
    openFppModal:function(component,event,helper){
        //event to notify Parent to openDiscountModal
        var eventComponent = component.getEvent("NotifyParentOpenFppModal");
        eventComponent.setParams({ "notifyParam":  component.get('v.lineItem')});
        eventComponent.setParams({ "notifyParam1": component.get('v.productDetail') });
        eventComponent.fire(); 
    },
        openDeliveryModal:function(component,event,helper){
        //event to notify Parent to openDiscountModal
        var eventComponent = component.getEvent("NotifyParentOpenDeliveryModal");
        eventComponent.setParams({ "notifyParam":  component.get('v.lineItem')});
        eventComponent.setParams({ "notifyParam1": component.get('v.productDetail') });
        eventComponent.fire(); 
    },
    handleDeliveryDateSelected : function(component, event, helper){
        var DM =  event.getParam("deliveryMode");
        var deliveryMode =  component.get("v.lineItem.Delivery_Mode__c");
        console.log("deliveryDateInSCLIC"+event.getParam("deliveryDate"));
        if(DM == deliveryMode)
           component.set("v.lineItem.DeliveryDate__c",event.getParam("deliveryDate"));  
    }
})