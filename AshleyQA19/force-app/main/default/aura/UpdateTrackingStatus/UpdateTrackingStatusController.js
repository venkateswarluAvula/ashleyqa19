({
    myAction : function(component, event, helper) {
        var getId = component.get("v.recordId");
        var isReload = component.get('c.getIsReload');
        isReload.setParams({
            recId : getId
        });
        isReload.setCallback(this,function(res){
             console.log('selectedItems::::res.getReturnValue-->',+res.getReturnValue());
             var result = res.getReturnValue();
             console.log('res--->',+result);
            if(result== '1')
            {
                console.log('CHANGES MADE');
                component.set("v.isSuccess", true);
                component.set("v.isReload",true);
                console.log('v.isReload-->'+component.get('v.isReload'));
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.isSuccess", false);
                    }), 10000
                );
                isReload = true;
            }
        }); 
        var getRecord = component.get('c.getRecord');
        getRecord.setParams({
            recId : getId
        });
        getRecord.setCallback(this,function(res){
            console.log('ReturnValue-->',+JSON.stringify(res.getReturnValue()));
            var result = res.getReturnValue();
             component.set("v.componentRecordName",result);
        });
        $A.enqueueAction(isReload);
        $A.enqueueAction(getRecord);
    },
	getStatus : function(component, event, helper) {
		console.log("recordLoader-->"+component.find("recordLoader").get("v.value"));
	    // Ask Lightning Data Service to save the record
	    var getId = component.get("v.recordId");
        var result = '1';
	  	var isReload = false;
        var isError = false;
        var result = '1';
        //----TRACKING NUMBER----START----//
        var isTracking = component.get('c.getTrackingNumber');
        isTracking.setParams({
            mynum : getId,
            FulfillerId : component.find('fulfillerID').get('v.value'),
            PoNum : component.find('POnumber').get('v.value'),
        });
        isTracking.setCallback(this,function(res){
             console.log('selectedItems::::res.getReturnValue-->',+res.getReturnValue());
             result = res.getReturnValue();
             console.log('isSavemySN--->',+result);
            if(result== '1')
        	{
                console.log('CHANGES MADE');
                document.location.reload(true);
            return;
           /*     component.set("v.isSuccess", true);
                component.set("v.isReload",true);
                console.log('v.isReload-->'+component.get('v.isReload'));
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.isSuccess", false);
                    }), 50000
                );
                isReload = true;
                */
           /*     console.log('CHANGES MADE');
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "UPDATED TRACKING NUMBER",
                    "message": "Tracking Number Updated.",
                	 type: "success",
                });
                resultsToast.fire();
                */
            }
            else
            {
                console.log('----NO CHANGE----');
                component.set("v.isFailed", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.isFailed", false);
                    }), 3000
                );
                return;
                console.log('----open----');
          /*    var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Warning",
                    "message": "Part Order Number Not Found",
                	 type: "warning",
                });
                resultsToast.fire();
              console.log('NO CHANGE');
              */
            }
            
        /*    var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": getId,
                    "slideDevName": "detail"
                });
                navEvt.fire(); */
         }); 
        
        $A.enqueueAction(isTracking);
       
        //----TRACKING NUMBER----END----//   
     },
    onCancel : function(component, event, helper) {
    
        // Navigate back to the record view
        var navigateEvent = $A.get("e.force:navigateToSObject");
        navigateEvent.setParams({ "recordId": component.get('v.recordId') });
        navigateEvent.fire();
    }
})