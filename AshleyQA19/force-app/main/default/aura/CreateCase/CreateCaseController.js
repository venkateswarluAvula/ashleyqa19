({
	showRequiredFields: function(component, event, helper){
		$A.util.removeClass(component.find("Status"), "none");
		$A.util.removeClass(component.find("Type"), "none");
		$A.util.removeClass(component.find("Sub-Type"), "none");
	},
    
	myAction : function(component, event, helper) {
        var getId = component.get("v.recordId");
		console.log('c2 rec id--' + getId);

		var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: "Create Case"
            });
            workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "action:new_case",
                iconAlt: "Create Case"
            });
        })
        .catch(function(error) {
            console.log(error);
        });

        var parentId;
        if (getId != null) {
            parentId = getId;
        } else {
            var pageRef = component.get("v.pageReference");
            console.log('pageRef: ' + JSON.stringify(pageRef));
            if (pageRef != null) {
                var ws = pageRef.state.ws;
                parentId = (ws.split("/", 5))[4];
                console.log('ws: ' + ws);
            }
            console.log('parentId: ' + parentId);
        }

        var newid = component.get("v.externalid");
        //console.log('c2 externalid-->'+newid);
        
        var endrecId = event.getParam("caserecid");
        console.log('c2 evt recid--' + endrecId);
        component.set ("v.getend_id", endrecId);
        //alert('my end record id2' + component.get("v.getend_id"));
        
		var actionSalesOrder = component.get("c.getSalesOrderInfo");
		actionSalesOrder.setParams({
			//"recordId" : getId
            "recordId" : parentId
		});
		actionSalesOrder.setCallback(this, function(response) {
			if (component.isValid() && response.getState() === "SUCCESS") {
				component.set("v.salesOrderObj", response.getReturnValue());
				console.log('salesorder: ' + JSON.stringify(response.getReturnValue()));
                
                 var legacyShipTo; //= '8888400-473';
                if (response.getReturnValue().fulfillerID__c == null){
                    component.set("v.accShipto1", response.getReturnValue().phhERPAccounShipTo__c);
                    legacyShipTo = response.getReturnValue().phhERPAccounShipTo__c;
                }
                else{
                    component.set("v.accShipto1", response.getReturnValue().fulfillerID__c);
                    //legacyShipTo = response.getReturnValue().fulfillerID__c;
                }
                
                if (legacyShipTo != null){
                    var actionmarketAcc = component.get("c.getMarketConfig");
                    actionmarketAcc.setParams({
                        "legacy" : legacyShipTo
                    });
                    actionmarketAcc.setCallback(this, function(responseMarketAcc) {
                        if (component.isValid() && responseMarketAcc.getState() === "SUCCESS") {
                            component.set("v.defaultMarket",responseMarketAcc.getReturnValue());
                           
                        } else {
                            var errorToast = $A.get("e.force:showToast");
                            errorToast.setParams({"message": responseMarketAcc.getError()[0].message, "type":"error"});
                            errorToast.fire();
                        }
                    });
                    $A.enqueueAction(actionmarketAcc);
                    
                }
                else{
                   component.set("v.defaultMarket",response.getReturnValue().phhMarketAccount__c); 
                    
                }
                console.log('defaultMarket: ' + component.get('v.defaultMarket'));
                console.log('accShipto: ' + component.get('v.accShipto1'));
				var actionAcc = component.get("c.getAccDetail");
				actionAcc.setParams({
					"accId" : response.getReturnValue().phhGuestID__c
				});
				actionAcc.setCallback(this, function(response1) {
					if (component.isValid() && response1.getState() === "SUCCESS") {
						console.log('accObj: ' + JSON.stringify(response1.getReturnValue()));
						component.set("v.accName", response1.getReturnValue().Name);
						component.set("v.accId", response1.getReturnValue().Id);
					} else {
		                var errorToast = $A.get("e.force:showToast");
		                errorToast.setParams({"message": response1.getError()[0].message, "type":"error"});
		                errorToast.fire();
					}
				});
				$A.enqueueAction(actionAcc);

				var actionSalesLine = component.get("c.getSoLineItemsBySoExternalId");
				actionSalesLine.setParams({
					"extId" : response.getReturnValue().ExternalId
				});
				actionSalesLine.setCallback(this, function(response2) {
					if (component.isValid() && response2.getState() === "SUCCESS") {
						console.log('so line item: ' + JSON.stringify(response2.getReturnValue()));
		                response2.getReturnValue().forEach(function(key){
		                	component.set("v.phdShipAddress1", key.phdShipAddress1__c);
		                	component.set("v.phdShipAddress2", key.phdShipAddress2__c);
		                	component.set("v.phdShipCity", key.phdShipCity__c);
		                	component.set("v.phdShipState", key.phdShipState__c);
		                	component.set("v.phdShipZip", key.phdShipZip__c);
		                });
                        
                        var actionStatePl = component.get("c.getStatePl");
                        actionStatePl.setCallback(this, function(response6) {
                            if (component.isValid() && response6.getState() === "SUCCESS") {
                                console.log('Picklist: ' + JSON.stringify(response6.getReturnValue()));
                                var opts = [];
                                opts.push({ class: "optionClass", label: "State", value: "State", disabled: "true" });
                                response6.getReturnValue().forEach(function(key){
                                    var pl = component.get("v.phdShipState");
                                    if((pl != null) && (pl == key))
                                    {
                                        opts.push({ class: "optionClass", label: key, value: key, selected: "true" });
                                    } else {
                                        opts.push({ class: "optionClass", label: key, value: key });
                                    }
                                });
                                console.log('opts: ' + JSON.stringify(opts));
                                component.find("addState").set("v.options", opts);
                            }
                            else {
                                var errorToast = $A.get("e.force:showToast");
                                errorToast.setParams({"message": response6.getError()[0].message, "type":"error"});
                                errorToast.fire();
                            }
                        });
                        $A.enqueueAction(actionStatePl);

		                var responseStr = JSON.stringify(response2.getReturnValue());
		                if (responseStr == '[]') {
		                	component.set("v.openInvoicedLineItem", false);
		                } else {
		                	component.set("v.openInvoicedLineItem", true);
		                }

						component.set("v.lineItem", response2.getReturnValue());
						component.set('v.mydata', response2.getReturnValue());
						component.set('v.options', [
							{label: 'Item Sku', fieldName: 'phdItemSKU__c', type: 'text'},
							{label: 'Description', fieldName: 'phdItemDesc2__c', type: 'text'},
							{label: 'Quantity', fieldName: 'phdQuantity__c', type: 'number'},
							{label: 'Delivery Type', fieldName: 'phdDeliveryType__c', type: 'text'},
							{label: 'Ship Zip', fieldName: 'phdShipZip__c', type: 'text'},
							{label: 'Delivered Date', fieldName: 'phdPurchaseDate__c', type: 'Datetime'},
							{label: 'Sales Status', fieldName: 'phdSaleType__c', type: 'text'},
							{label: 'Id', fieldName: 'Id', type: 'Id'},
						]);
					}
					else{
		                var errorToast = $A.get("e.force:showToast");
		                errorToast.setParams({"message": response2.getError()[0].message, "type":"error"});
		                errorToast.fire();
					}
				});
				$A.enqueueAction(actionSalesLine);

			} else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                errorToast.fire();
			}
		});
		$A.enqueueAction(actionSalesOrder);
	},

	sectionOne : function(component, event, helper) {
		helper.helperFun(component, event, 'articleOne');
	},

	dosave : function(component, event, helper) {
		var selectedItems = component.get("v.selectedvals");
		var checkvalue = component.find("quoteField");
		var selectid = component.get("v.lineItem.Id");
		var flag = new Boolean(true);
		//var subjectval = component.find('Subject').get('v.value');
		var descriptionval = component.find('Description').get('v.value');
		var originval = component.find('Origin').get('v.value');
		var refusalreasonval = component.find('Refusal_Reason').get('v.value');
		var statusval = component.find('Status').get('v.value');
		var typeval = component.find('Type').get('v.value');
		var subtypeval = component.find('Sub-Type').get('v.value');
        var servProvider = component.find('ServiceProvider').get('v.value');
        
        var subjectval = typeval +' : '+ subtypeval;
        if(typeval == 0 || statusval == 0 || subtypeval == 0 || $A.util.isEmpty(subjectval) || $A.util.isEmpty(descriptionval) || subjectval == 0){
            flag = false;
        }		
        
		var isError = false;
		if(flag == true){
            
            var defaultMkt = component.get("v.defaultMarket");
            
            var newMkt = component.find('Market').get('v.value');
            console.log('defaultMkt'+defaultMkt+'newMkt'+newMkt);
            
            if(defaultMkt != newMkt){
                component.set("v.ValidationError" , true);
                isError = true;
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.ValidationError", false);
                    }), 8000
                );
            }
            
            if((subtypeval == 'Home Damage') && ($A.util.isEmpty(servProvider))){
                component.set("v.ValidationError3" , true);
                isError = true;
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.ValidationError3", false);
                    }), 8000
                );
            }
            			
			var isValidEmail = true;
			var emailField = component.find("leadEMail");
			var emailFieldValue = emailField.get("v.value");
			var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
			if(!$A.util.isEmpty(emailFieldValue)){
				if(emailFieldValue.match(regExpEmailformat)){
					emailField.set("v.errors", [{message: null}]);
					$A.util.removeClass(emailField, 'slds-has-error');
					isValidEmail = true;
				}else{
					$A.util.addClass(emailField, 'slds-has-error');
					emailField.set("v.errors", [{message: "Please Enter a Valid Email Address"}]);
					isError = true; 
					isValidEmail = false;
				}
			}

			var inputCmp = component.find("inputCmp");
			var value = inputCmp.get("v.value");
			if(component.get("v.caseIs.SuppliedPhone") != undefined && value!=''){
				var isValidPhone = true; 
				var phoneField = component.find("inputCmp");
				var phoneFieldValue = phoneField.get("v.value");
				var phoneformat = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;  
				if(!$A.util.isEmpty(phoneFieldValue)){
					if(phoneFieldValue.match(phoneformat)){
						phoneField.set("v.errors", [{message: null}]);
						$A.util.removeClass(emailField, 'slds-has-error');
						isValidPhone = true;
					}else{
						$A.util.addClass(phoneField, 'slds-has-error');
						phoneField.set("v.errors", [{message: "Please Enter a Valid Phone Number"}]);
						isError = true; 
						isValidPhone = false;
					}
				}
			}
           var pref = component.find('select').get('v.value');
            if((pref == '') || $A.util.isEmpty(pref)){
                component.set("v.ValidationError4" , true);
                isError = true;
             }
            else{
                component.set("v.ValidationError4" , false);
            }
            
			if(isError== true){
				component.set("v.isError3", true);
				window.setTimeout(
					$A.getCallback(function() {
						component.set("v.isError3", false);
					}), 3000
				);
				return;
			}

            console.log('accShipto: ' + component.get('v.accShipto1'));
            
			//create case
			var action1 = component.get('c.create_case');
			action1.setParams({
				'caseis':component.get('v.caseIs'),
				'salesorder':component.find('Sales_Order_id').get('v.value'),
				'accountId':component.get('v.accId'),
				'casetype':typeval,
                'NPS':component.get('v.salesOrderObj').Text_consent__c, 
                'casesubtype':subtypeval,
                'subject':subjectval,
				'marketAccount':defaultMkt,
                'serviceProvider':servProvider,
				'storeNameStoreNumberPC':component.get('v.salesOrderObj').phhStoreNameStoreNumberPC__c,
				'shipAddress1':component.get('v.phdShipAddress1'),
				'shipAddress2':component.get('v.phdShipAddress2'),
				'shipCity':component.get('v.phdShipCity'),
				'shipState':component.get('v.phdShipState'),
				'shipZip':component.get('v.phdShipZip'),
                'preferred':component.find('select').get('v.value'),
				'caseorigin':originval,
				'caserefusal':refusalreasonval,
				'casestatus':statusval,
                'accShipto':component.get('v.accShipto1')
            });
            /* 'resolved':typeofresolval,*/
			action1.setCallback(this,function(res){
				console.log('case: ' + res.getState());
				if (component.isValid() && res.getState() === "SUCCESS") {
					console.log('case: ' + JSON.stringify(res.getReturnValue()));
					var caseId =  res.getReturnValue();
					console.log('caseId: ' + caseId);

					if (caseId != null) {
						//if the line items are selected then insert in product line item
						if(selectedItems != null && selectedItems != '') {
							console.log('selected items');
							var action2 = component.get('c.newProductLineItemRecord');
							action2.setParams({
								'caseId':caseId,
								'ProductId': selectedItems,
								'salesorder':component.find('Sales_Order_id').get('v.value')
							});
							var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								title: "Success!",
								message: "Successfully created Case and Product Line Items!",
								type: "success"
							});
							toastEvent.fire();
							$A.enqueueAction(action2);
                            //----closing creation tab after saving----
                            helper.closingtab(component);
                            
						} else {
							console.log('No selected line item');
							var toastEvent = $A.get("e.force:showToast");
							toastEvent.setParams({
								title: "Success!",
								message: "Successfully submitted Case!",
								type: "success"
							});
							toastEvent.fire();
                            //----closing creation tab after saving----
                            helper.closingtab(component);
                            
						}
						var navEvt = $A.get("e.force:navigateToSObject");
						navEvt.setParams({
							"recordId": caseId,
							"slideDevName": "detail"
						});
						navEvt.fire();
					} else {
						//error while creating case
		                var errorToast = $A.get("e.force:showToast");
		                errorToast.setParams({"message": "Error while creating case, please contact admin.", "type":"error"});
		                errorToast.fire();
					}
                    
				}
				else{
	                var errorToast = $A.get("e.force:showToast");
	                errorToast.setParams({"message": res.getError()[0].message, "type":"error"});
	                errorToast.fire();
				}
                
			});
			$A.enqueueAction(action1);
            
		}
		else{
			component.set("v.isError2", true);
			//  document.getElementById('error').innerHTML="Enter all the mandatory Fields";
			window.setTimeout(
				$A.getCallback(function() {
					component.set("v.isError2", false);
				}), 3000
			);
			return;
		}
        
	},

	//Get selected product line item
	getSelectedVal:function(component,event) {
		var selectedRows = event.getParam('selectedRows');
		var selectedListlength = selectedRows.length;
		var selectedDesc = [];
		var selectedvals = [];
		var finallist = [];
		var flag = 0;
		for (var i = 0; i < selectedRows.length; i++){
			component.set("v.selectedzip"+ selectedRows[i].phdShipZip__c);
			var selctedzip = selectedRows[i].phdShipZip__c;
			var selectDescription = selectedRows[i].phdItemDesc2__c;
			var selectedvals = selectedRows[i].Id;
			component.set("v.selectedvals" , selectedvals);
			finallist.push(selectedvals);
		}
		component.set("v.selectedvals" , finallist);
		for (var i = 0; i < selectedvals.length; i++){
			console.log('My selected vals' + selectedvals[i]);
		}
	},

	doCancel :function(component, event, helper) {		
        
        //while cancelling close the sub tab
        helper.closingtab(component);
		
	},

	onControllerFieldChange: function(component, event, helper) {
		alert(event.getSource().get("v.value"));
		// get the selected value
		var controllerValueKey = event.getSource().get("v.value");

		// get the map values   
		var Map = component.get("v.depnedentFieldMap");

		// check if selected value is not equal to None then call the helper function.
		// if controller field value is none then make dependent field value is none and disable field
		if (controllerValueKey != '--None--') {
			// get dependent values for controller field by using map[key].  
			// for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
			// map['India'] = its return all dependent picklist values.
			var ListOfDependentFields = Map[controllerValueKey];
			helper.fetchDepValues(component, ListOfDependentFields);
		} else {
			var defaultVal = [{
				class: "optionClass",
				label: '--None--',
				value: '--None--'
			}];
			component.find('conState').set("v.options", defaultVal);
			component.set("v.isDependentDisable", true);
		}
	},

	// function call on change the Dependent field    
	onDependentFieldChange: function(component, event, helper) {
		alert(event.getSource().get("v.value"));
	},
    
    // EDQ added Validation logic
    handleSearchChange: function (component, event, helper) {
        var settings = helper.getSettings();        
        helper.handleSearchChange(component, event, settings);
        helper.handleValidationStatus(component, settings);       
    },
    // EDQ added Validation logic
    handleSuggestionNavigation: function (component, event, helper) {
        var settings = helper.getSettings();
        var keyCode = event.which;
        if (keyCode == 13) { // Enter
            helper.acceptFirstSuggestion(component, settings);
        } else if (keyCode == 40) { //Arrow down
            helper.selectNextSuggestion(component, null, settings);
        } else if (keyCode == 38) { //Arrow up
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // EDQ event listener
    onSuggestionKeyUp: function (component, event, helper) {
        var settings = helper.getSettings();
        helper.onSuggestionKeyUp(component, event, settings);
    },
    // EDQ event listener
    handleResultSelect: function (component, event, helper) {
        var settings = helper.getSettings();
        helper.handleResultSelect(component, event, settings);
    },
    // EDQ event listener
    onAddressChanged: function (component, event, helper) {
        var settings = helper.getSettings();
        helper.handleValidationStatus(component, settings);
    },
    // EDQ event listener
    onElementFocusedOut: function (component, event, helper) {
        var settings = helper.getSettings();
        var useHasNotSelectedASuggestion = false;
        if (helper.isNull(event.relatedTarget)) {
            useHasNotSelectedASuggestion = true;
        } else if (helper.isNull(event.relatedTarget.id)) {
            useHasNotSelectedASuggestion = true;
        } else if (event.relatedTarget.id.indexOf(settings.suggestionIndexClassPrefix) === -1) {
            useHasNotSelectedASuggestion = true;
        }
        if (useHasNotSelectedASuggestion) {
            helper.hideAndRemoveSuggestions(component, settings);
        }
    }

});